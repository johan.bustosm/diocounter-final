// import {createStore, applyMiddleware} from 'redux'
import {createStore} from 'redux'
function saveStorage(state){
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state',serializedState)
  } catch (e) {
    console.log(e)
  }
}
function loadStorage(){
  try {
    const serializedState =  localStorage.getItem('state')
    if(serializedState === null){
      return({menu:{items:[]},token:"",project:{},step:1})
    }else{
      return JSON.parse(serializedState)
    }
  } catch (e) {
    console.log(e)
  }
}

const reducer = (state,action)=>{
  switch (action.type) {
    case 'SING_IN':
      return {
          ...state,
          menu:action.menu,
          token:action.token
      }
    case 'PROJECT':
    return {
        ...state,
        project:action.project,
        menu:action.menu
    }
    case 'RETURNPROJECT':
    return {
        ...state,
        project:"",
        menu:action.menu
    }
    case 'CONFIG_APP':
    return{
      ...state,
      step:action.step,
      temp:action.temp,
    }
    case 'NEW_PHOTO':
    return{
      ...state,
      img:action.imagen
    }

    default:
      return state
  }
}
// const logger = store => next => action => {
//   console.log('dispatching', action)
//   let result = next(action)
//   console.log('next state', store.getState())
//   return result
// }
const persistenceStore = loadStorage ()
// const store = createStore(reducer,persistenceStore,applyMiddleware(logger));
const store = createStore(reducer,persistenceStore);

store.subscribe(()=> saveStorage(store.getState()) )
export default  store
