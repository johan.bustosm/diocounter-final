import React, { Component } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';
// sidebar nav config
// import navigation from '../../_nav';
// routes config
import store from '../../store.js'
import routes from '../../routes';
import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';
import "./defaultlayout.css"

class DefaultLayout extends Component {
  constructor(){
    super()
    this.state={
      navigation:{items:[]},
      token:""
    }
    store.subscribe(()=>{
      this.change(store.getState())
    //   console.log("Entre por que hubo un cambio en dispatch ::: ",store.getState().menu)
    //   this.setState({
    //     token:"johan es un pro"
    //   })
    // this.setState({
    //   navigation:store.getState().menu,
    //   token:store.getState().token
    // });
    // console.log("cambiando estado ::: ", this.state.navigation)
    // console.log("cambiando estado ::: ", this.state.token)
  });
  }
  change(data){
    this.setState({
        token:"johan es un pro"
      })
  }
  render() {
    // console.log("render de menu :::", this.state.navigation)
    // console.log("render de token :::", this.state.token)
    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <AppSidebarNav navConfig={store.getState().menu} {...this.props} />
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes} />
            <Container fluid>
              <Switch>
                {routes.map((route, idx) => {
                    return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                        <route.component {...props} />
                      )} />)
                      : (null);
                  },
                )}
                <Redirect from="/" to="/login" />
              </Switch>
            </Container>
          </main>
          <AppAside fixed hidden>
            <DefaultAside />
          </AppAside>
        </div>
        <AppFooter>
          <DefaultFooter />
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
