import React, { Component } from 'react';
//import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem, NavLink, Tooltip } from 'reactstrap';
import { Row, Col, Button, DropdownItem, DropdownMenu, DropdownToggle, Nav, Tooltip,Modal, ModalHeader, ModalBody, ModalFooter,
 Input, InputGroup, InputGroupAddon, InputGroupText, FormFeedback, Label, Form, FormGroup, Alert } from 'reactstrap';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom'
import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import request from 'superagent';
import store from '../../store.js'
import env from "../../env.json";
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/robot.svg'
import "./DefaultHeader.css"
const expPass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.logoutUser = this.logoutUser.bind(this);
    this.state = {
      token:store.getState().token,
      tooltipOpen: false,
      logout:false,
      modalEditUser:false,
      modalEditPass:false,
      nowPass:"",
      password:"",
      passwordR:"",
      expPassV:false,
      expPasV:false,
      firtsName:"",
      lastName:"",
      viewAlert:false,
      messageAlert:"",
      colorAlert:""
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }
  toggleEditPass() {
    this.setState({
      modalEditPass: !this.state.modalEditPass,
      nowPass:"",
      password:"",
      passwordR:"",
      expPassV:false,
      expPasV:false,
      viewAlert:false,
    });
  }
  toggleEditUser() {
    this.setState({
      modalEditUser: !this.state.modalEditUser,
      firtsName:sessionStorage.getItem("first"),
      lastName:sessionStorage.getItem("last"),
    });
  }
  logoutUser(){
    localStorage.clear();
    sessionStorage.clear()


    this.setState({
      logout:!this.state.logout
    })
  }
  handleChangefirtsName(event){this.setState({firtsName:event.target.value}) }
  handleChangelastName(event){this.setState({lastName:event.target.value}) }
  handleChangeNowPass(event){this.setState({nowPass:event.target.value}) }
  handleChangePass(event){this.setState({password:event.target.value})
  if(expPass.test(event.target.value)===false){
    this.setState(
      {
        expPasV :true
      }
    )
  }else{
    this.setState(
      {
        expPasV:false
      }
    )
  }
}

handleChangePassRe(event){this.setState({   passwordR:event.target.value}) }
sendChagePassword=()=>{
  this.setState((prevState, props) => {
    return ({
      viewAlert:false,
      messageAlert:"",
      colorAlert:""
    });
  });
 if(this.state.password==="" || this.state.nowPass==="" || this.state.passwordR===""){
   this.setState((prevState, props) => {
   return ({
       viewAlert:true,
       messageAlert:"Campos vacios",
       colorAlert:"info",
     });
   });
 }else{
   var count = 0
   if(this.state.passwordR===this.state.password){
     this.setState(
       {
         expPassV :false
       }
       )
   }else{
     this.setState(
       {
         expPassV:true
       }
     )
     console.log("password");
     count = count +1
   }

    if(count>=1){
      return true
    }else{
      var joinUser = 'token ' + this.state.token
      request
      .post(env.http+"newpass")
      .set('authorization', String(joinUser))
      .send({pass:this.state.password , nowPass:this.state.nowPass })
      .end((err, res) => {
        console.log(res);
        if(res===undefined || res.status===500){
          this.setState((prevState, props) => {
            return ({
              viewAlert:true,
              messageAlert:"Error en el servidor, intentelo más tarde",
              colorAlert:"warning"
            });
          });
        }else{
          if(res.status===200){
            this.setState((prevState, props) => {
              return ({
                modalEditPass:false,
                messageAlert:"",
                viewAlert:false
              });
            });
          }else if(res.status===400){
            this.setState((prevState, props) => {
              return ({
                viewAlert:true,
                messageAlert:"Error, contraseña actual no es valida.",
                colorAlert:"danger"
              });
            });
          }
        }
      });
    }
 }
}

sendChageUser=()=>{
 if(this.state.firtsName==="" || this.state.lastName===""){
   this.setState((prevState, props) => {
   return ({
       viewAlert:true,
       messageAlert:"Campos vacios",
       colorAlert:"info",
     });
   });
 }else{
      var joinUser = 'token ' + this.state.token
      request
      .post(env.http+"changedateuser")
      .set('authorization', String(joinUser))
      .send({firts:this.state.firtsName, last:this.state.lastName})
      .end((err, res) => {
        console.log(res);
        if(res===undefined || res.status===500){
          this.setState((prevState, props) => {
            return ({
              viewAlert:true,
              messageAlert:"Error en el servidor, intentelo más tarde",
              colorAlert:"warning"
            });
          });
        }else{
          if(res.status===200){
            this.setState((prevState, props) => {
              return ({
                modalEditUser:false,
                messageAlert:"",
                viewAlert:false
              });
            });
            sessionStorage.setItem("name",this.state.firtsName+" "+this.state.lastName)
            sessionStorage.setItem("first",this.state.firtsName)
            sessionStorage.setItem("last",this.state.lastName)

          }
        }
      });

 }
}


  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;
    if(this.state.logout){
        return(<Redirect to='/login'/>)
    }
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: "./logointelligexLogin.png", width: 120, height: 50, alt: 'Intelligex Logo' }}
          minimized={{ src: sygnet, width: 30, height:50, alt: 'Intelligex Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
      {
         //
         //  <NavItem className="d-md-down-none">
         //    <NavLink href="#"><i className="icon-bell"></i><Badge pill color="danger">5</Badge></NavLink>
         //  </NavItem>
         //  <NavItem className="d-md-down-none">
         //    <NavLink href="#"><i className="icon-list"></i></NavLink>
         //  </NavItem>
         //  <NavItem className="d-md-down-none">
         //    <NavLink href="#"><i className="icon-location-pin"></i></NavLink>
         //  </NavItem>
        }

          <AppHeaderDropdown  direction="down">
          <DropdownToggle nav>
            <img id = "user" src={'assets/img/avatars/user.jpg'} className="img-avatar" alt="Avatar Users" />
          </DropdownToggle>
          <DropdownMenu right style={{ right: 'auto' }} className="targetMenu">
            <DropdownItem header tag="div" className="text-center"><strong>Ajustes</strong></DropdownItem>

            <div className="btfalse">
              <Row >
                <Col md="8">
                  <i className="fa fa-user iconoMenu"></i> Perfil
                </Col>
                <Col  md="4" className="text-right">
                  <Button color="secondary" size="sm" onClick={()=>this.toggleEditUser()}>Editar</Button>
                </Col>
              </Row>
            </div>
            <div className="btfalse">
              <Row >
                <Col md="8" >
                  <i className="fa fa-lock iconoMenu"></i> Seguridad
                </Col>
                <Col md="4" className="text-right">
                  <Button color="secondary" size="sm" onClick={()=>this.toggleEditPass()}>Editar</Button>
                </Col>
              </Row>
            </div>
            <DropdownItem divider />
            <DropdownItem  onClick={this.logoutUser}><i className="material-icons iconlogout">power_settings_new</i> Cerrar sesión</DropdownItem>
          </DropdownMenu>
          <Tooltip placement="right" isOpen={this.state.tooltipOpen} target={"user"} toggle={this.toggle}>
        <div>{sessionStorage.getItem("name")}<br/>{sessionStorage.getItem("email")}  </div>

        </Tooltip>
        </AppHeaderDropdown>

        </Nav>

        {/*<AppAsideToggler className="d-lg-none" mobile />*/}

        {
          /*
            Modal para editar Contraseña
          */
        }
      <Modal isOpen={this.state.modalEditPass}  className={this.props.className}>
        <ModalHeader>Cambiar contraseña</ModalHeader>
        <ModalBody>
        <Label for="description">Contraseña actual</Label>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-lock"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input  type="password" onCopy="return false" placeholder="Contraseña actual" value={this.state.nowPass} onChange={this.handleChangeNowPass.bind(this)}  />
        </InputGroup>
        <Label for="description">Nueva contraseña</Label>
        <InputGroup className="mb-3">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>
              <i className="icon-lock"></i>
            </InputGroupText>
          </InputGroupAddon>
          <Input  type="password" onCopy="return false" invalid={this.state.expPasV}  placeholder="Nueva contraseña" value={this.state.password} onChange={this.handleChangePass.bind(this)}  />
          <FormFeedback>La contraseña debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.</FormFeedback>
        </InputGroup>
        <Label for="description">Repetir nueva contraseña</Label>
          <InputGroup className="mb-4">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>
                <i className="icon-lock"></i>
              </InputGroupText>
            </InputGroupAddon>
            <Input type="password"  onpaste="return false"  invalid={this.state.expPassV} placeholder="Repetir nueva contraseña" value={this.state.passwordR} onChange={this.handleChangePassRe.bind(this)} />
            <FormFeedback>No coinciden las contraseñas</FormFeedback>
          </InputGroup>
          <Alert color={this.state.colorAlert} isOpen={this.state.viewAlert}>
           {this.state.messageAlert}
         </Alert>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.sendChagePassword}>Modificar</Button>
          <Button color="secondary" onClick={()=>this.toggleEditPass()}>Cancelar</Button>
        </ModalFooter>
      </Modal>

      {
        /*
          Modal para editar Usuarios
        */
      }
      <Modal isOpen={this.state.modalEditUser}  className={this.props.className}>
        <ModalHeader>Datos de usuario</ModalHeader>
        <ModalBody>
        <Form>
          <FormGroup>
            <Label for="name">Nombre</Label>
            <Input invalid={this.state.expNameTempV}  maxlength="20"  onChange={this.handleChangefirtsName.bind(this)} type="text" name="name" id="name" value={this.state.firtsName} />
            <FormFeedback>Caracter invalido</FormFeedback>
          </FormGroup>
          <FormGroup>
            <Label for="description">Apellido</Label>
            <Input invalid={this.state.expDescTempV} onChange={this.handleChangelastName.bind(this)} type="text" name="description" id="description" value={this.state.lastName} />
            <FormFeedback>Caracter invalido</FormFeedback>
          </FormGroup>
        </Form>
        <Alert color={this.state.colorAlert} isOpen={this.state.viewAlert}>
         {this.state.messageAlert}
       </Alert>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={this.sendChageUser}>Modificar</Button>
          <Button color="secondary" onClick={()=>this.toggleEditUser()}>Cancelar</Button>
        </ModalFooter>
      </Modal>
        </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
