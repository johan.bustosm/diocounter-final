/*
Dependencias
*/
import React, { Component } from 'react';
import {
  Alert,
  Button,
  Form,
  FormGroup,
  FormFeedback,
  Label,
  Input,
  Row,
  Col,
  Card,
  Modal, ModalHeader, ModalBody, ModalFooter,
  CardTitle,
  ButtonGroup,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,DropdownItem
} from 'reactstrap';

import request from 'superagent'
import store from '../../store.js'
import navigation from '../../_nav'
import {Link} from 'react-router-dom'
import Titletarget from './titletarjet.js'
import navigation2 from '../../_nav2'
import env from "../../env.json"
import "./css/project.css"

const expName= /^[a-zA-ZñÑáéíóúÁÉÍÓÚ 0-9]*$/;


class Proyect extends Component {
  constructor() {
    super();
    this.state = {
      name:"",
      description:"",
      expNameV:false,
      expDescV:false,
      expNameTempV:false,
      expDescTempV:false,
      projects:[],
      token:store.getState().token,
      modal:false,
      modalAdver:false,
      modalEdit:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      idtemp:"",
      nametemp:"",
      descrtemp:"",
      alert:false,
      messageAlert:"",
      colorAlert:"",
      tooltipOpen: false
    };
    this.toggle = this.toggle.bind(this);
    this.toggle2 = this.toggle2.bind(this);
   }
   toggle() {
     this.setState({
       modal: !this.state.modal
     });
   }
   imagesIcon() {
       if (this.state.iconmodal === "info") {
         return ( < i className = "material-icons infomo" > info < /i>)
         }
         else if (this.state.iconmodal === "warning") {
           return ( < i className = "material-icons icowarni" > warning < /i>)
           }
           else if (this.state.iconmodal === "check_circle_outline") {
             return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
             }else if(this.state.iconmodal==="error_outline"){
               return (<i className="material-icons iconerror">error_outline</i>)
             }
   }

   componentDidMount(){
     store.dispatch({
       type:"RETURNPROJECT",
       menu:navigation2
     })
     sessionStorage.setItem("menu",navigation2)
     this.projects();
   }
   /*
   Capturando informacion de los iput-text para crear un nuevo proyecto
   */
    handleChangeName(event){

      this.setState({name:event.target.value})
   }
   handleChangeDescrip(event){this.setState({   description:event.target.value})}
   handleChangeNametemp(event){this.setState({nametemp:event.target.value})}

 acepProyEdit=()=>{
   this.setState(
     {
       alert :false,
       expNameTempV :false,
        expDescTempV:false
     }
   )
   if(this.state.nametemp==="" || this.state.descrtemp==="" ){
     this.setState({
       alert:true,
       messageAlert:"Debe llenar todos los datos.",
       colorAlert:"warning"
     })
     return true
   }
   var count = 0;
   if(expName.test(this.state.nametemp)===false){
     this.setState(
       {
         expNameTempV :true
       }
     )
     count = count+1
   }else{
     this.setState(
       {
         expNameTempV :false
       }
     )
   }
   if(expName.test(this.state.descrtemp)===false){
     this.setState(
       {
         expDescTempV:true
       }
     )
      count = count+1
   }else{
     this.setState(
       {
         expDescTempV:false
       }
     )
   }
   if(count>=1){
     return true
   }else{
     var user = sessionStorage.getItem("token");
     var joinUser = 'token ' + user
     request
       .post(env.http + "ediproject")
       .set('authorization', String(joinUser))
       .send({
         name: this.state.nametemp,
         idProject: this.state.idtemp,
         description:this.state.descrtemp
       })
       .end((err, res) => {
         // console.log(res);
         if (err) {
           // console.log(err)

         } else {

           if (res.status === 200) {
             this.setState({
                expNameTempV:false,
                expDescTempV:false,
                modalEdit:false,
                idtemp:"",
                nametemp:"",
                descrtemp:""
             })
             this.projects()
           }else if(res.status===201){
             this.setState({
               alert:true,
               messageAlert:"Existe ya un proyecto nombrado "+this.state.nametemp+", debe cambiarlo para actualizar el nombre actual. ",
               colorAlert:"info"
             })

           }
           else if(res.status===202 || res.status===500){
             this.setState({
               alert:true,
               messageAlert:"Existe un error con el servidor, intentelo más tarde.",
               colorAlert:"danger"
             })
           }
         }

       });
   }
 }
 cancelEdit=()=>{
   this.setState({
     modalEdit:false,
     nametemp:"",
     descrtemp:"",
     expNameTempV:false,
     expDescTempV:false,
     alert :false,
   })
 }

   handleChangeDescriptemp(event){this.setState({   descrtemp:event.target.value})}
   /*
    Peticiones requesHttp
   */
   newProject=()=>{
         if(this.state.name==="" || this.state.description===""){
           this.setState({
             modal: true,
             titlemodal:"Usuario",
             bodymodal:"Señor usuario, debe llenar todos los campos.",
             iconmodal:"warning"
           })

     }else{
       var count = 0
       if(expName.test(this.state.name)===false){
         this.setState(
           {
             expNameV :true
           }
         )
         count = count+1
       }else{
         this.setState(
           {
             expNameV :false
           }
         )
       }
       if(expName.test(this.state.description)===false){
         this.setState(
           {
             expDescV:true
           }
         )
          count = count+1
       }else{
         this.setState(
           {
             expDescV:false
           }
         )
       }
       if(count>=1){
         return true
       }else{

         var user = sessionStorage.getItem("token");
         // var user = this.state.token
         var joinUser ='token '+user
         request
         .post(env.http+"project")
         .set('authorization',String(joinUser))
         .send({ name: this.state.name , description: this.state.description }) // sends a JSON post body
         .end((err, res) => {
           if (err) {

           }else{

             if(res.body.code===200){
               this.setState({
                 name:"",
                 description:"",
                 expDescV:false,
                 expNameV:false
               })
               this.projects();
             }else if(res.status===201){
               this.setState({
                 modal: true,
                 titlemodal:"Usuario",
                 bodymodal:"Señor usuario, debe cambiar el nombre del proyecto porque ya esta registrado el ingresado.",
                 iconmodal:"warning"
               })
             }
           }
         });
       }
     }
   }

   projects=()=>{
     var user = sessionStorage.getItem("token");
     var joinUser ='token '+user
     request
     .get(env.http+"project")
     .set('authorization',String(joinUser))
     .then(res => {
       //console.log(res.body)
        this.setState({
          projects:res.body.message
        })
   })
   .catch(err => {
console.log("Error::: ",err);
   });
   }

   deleProject=(idProject)=>{
     this.setState({
       modalAdver: true,
       titlemodal:"Usuario",
       bodymodal:"Señor usuario, esta seguro de eliminar el proyecto.",
       iconmodal:"warning",
       idtemp:idProject
     })
   }
   editProject=(idProject,name,descript)=>{
     this.setState({
       modalEdit: true,
       nametemp:name,
       descrtemp:descript,
       idtemp:idProject
     })
   }

   cancelDele=()=>{
     this.setState({
       modalAdver: false,
     })
   }

   acepConfiDele=()=>{
     this.setState({
       modalAdver: false,
     })
     var user = sessionStorage.getItem("token");
     // var user = this.state.token
     var joinUser ='token '+user
     request
      .post(env.http+"deleproject")
      .set('authorization',String(joinUser))
      .send({ project_id: this.state.idtemp }) // sends a JSON post body
      .end((err, res) => {
        if (err) {

        }else{

          if(res.body.code===200){
            this.projects();
          }
        }
    });

   }

   handleButton=(key)=>{
     // console.log("key :::",key)

   }
   changeState(obj){
     store.dispatch({
       type:"PROJECT",
       project:{name:obj.name,id:obj._id},
       menu:navigation
     })
     sessionStorage.setItem("menu",navigation)
   }
   toggle2() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }

name=(namePro)=>{
   
    if(namePro.length>13){
      return (namePro[0].toUpperCase()+namePro.slice(1, 10))+"..."
    }else{
      return (namePro[0].toUpperCase()+namePro.slice(1,namePro.length))
    }
  }

toogleSettin=(key)=>{
    this.state[key] = !this.state[key]
    this.setState({
      key : ! this.state[key]
    })
  }

   render(){
     sessionStorage.removeItem("temp")
     var data = " "
     if(this.state.projects.length === 0){


        data = (<Col className="mt-4">
            <Alert color="dark">

            <p className="noteP">Señor usuario, actualmente no cuenta con ningún proyecto recuerde que debe registrar uno nuevo.</p>
            </Alert>
         </Col>)

     }else {

     data = this.state.projects.map((obj,index)=>{

       const newTo = {
                      pathname: "/dash",
                      param1: {name:obj.name,id:String(obj._id)}
                      };
           return(
          <Col sm="3" md="3" key={index}>
             <Card body className="p-3 sombra">
               <Row>
               <Col className="md-10">
      
               </Col>

               </Row>
               <CardTitle id={index}>{this.name(obj.name)}
      <ButtonGroup className="float-right" style={{"border":"0px"}}>
                   <ButtonDropdown id={`card${index}`} isOpen={this.state[`card${index}`] == undefined ? this.state[`card${index}`]= false :  this.state[`card${index}`]} toggle={() => { this.toogleSettin(`card${index}`) }}>
                         <DropdownToggle caret className="p-0 button-dropmenu" color="white" > 
                         <i className="icon-settings animate-trasla "></i>                      
                         </DropdownToggle>
                         <DropdownMenu right>
                         <DropdownItem onClick={()=>this.editProject(obj._id,obj.name,obj.description)}>
                           <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>edit</i>
                           Editar
                           </DropdownItem>
                           <DropdownItem onClick={()=>this.deleProject(obj._id)}>
                           <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>delete_outline</i>
                           Eliminar
                           </DropdownItem>
                         </DropdownMenu>
                       </ButtonDropdown>
                     </ButtonGroup>
      </CardTitle>
               <Titletarget name={obj.name} index={index} nameC={obj.name[0].toUpperCase()+obj.name.slice(1,obj.name.lenth)} description={obj.description}             
               />
           

                <Row>
                  <Col className="text-left" >
                      {/* <Link to={newTo }><i className="material-icons iconosintro " onClick={()=>this.changeState(obj)}>input</i></Link> */}
                  </Col>
                  <Col className="text-center">
                  {/* <i className="material-icons iconoseditpro" onClick={()=>this.editProject(obj._id,obj.name,obj.description)}>edit</i> */}
                  </Col>
                  <Col className="text-right">
                  {/* <i className="material-icons iconosdelete" onClick={()=>this.deleProject(obj._id)}>delete_outline</i> */}
                  <Link to={newTo }><i className="material-icons iconosintro " onClick={()=>this.changeState(obj)}>input</i></Link> 
                  </Col>
                </Row>
             </Card>
           </Col>
       )
     })
   }
     return(
       <div>
         <Row>
           <Col className="text-center  mb-3">  <span className="display-4 ">Proyectos</span> </Col>
         </Row>
         <Row>
         <Col >
         <Card body className="sombra">
         <Form  >
         <div className="form-row">
           <div className="form-group col-md-5">
            <Label for="name" className="">Nombre</Label>
            <Input invalid={this.state.expNameV} className="nametext" maxlength="20" type="text" name="name" id="name" placeholder="Nombre del proyecto" value={this.state.name} onChange={this.handleChangeName.bind(this)}/>
            <FormFeedback>Caracter invalido</FormFeedback>
           </div>
           <div className="form-group col-md-5">
           <Label for="description" className="mr-sm-2">Descripción</Label>
           <Input invalid={this.state.expDescV} type="textarea" name="description" id="description" placeholder="Descripción del proyecto" value={this.state.description} onChange={this.handleChangeDescrip.bind(this)}/>
           <FormFeedback>Caracter invalido</FormFeedback>
           </div>

           <div className="col-md-2 mt-3 ">
           <br/>
           <Button id="button" color="primary" size="lg" onClick={this.newProject}> Nuevo</Button>
           </div>
         </div>


        </Form>
        </Card>
        </Col>
        </Row>
        <Row>
        {data}
        </Row>
     
        <div>
          <Modal isOpen={this.state.modal}  className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal isOpen={this.state.modalAdver}  className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.acepConfiDele}>Aceptar</Button>
              <Button color="secondary" onClick={this.cancelDele}>Cancelar</Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal isOpen={this.state.modalEdit}  className={this.props.className}>
            <ModalHeader>Actualización de datos</ModalHeader>
            <ModalBody>
              <Row>
              <Col>
              <Form>
                <FormGroup>
                  <Label for="name">Nombre</Label>
                  <Input invalid={this.state.expNameTempV}  maxlength="20"  onChange={this.handleChangeNametemp.bind(this)} type="text" name="name" id="name" value={this.state.nametemp} />
                  <FormFeedback>Caracter invalido</FormFeedback>
                </FormGroup>
                <FormGroup>
                  <Label for="description">Descripción</Label>
                  <Input invalid={this.state.expDescTempV} onChange={this.handleChangeDescriptemp.bind(this)} type="textarea" name="description" id="description" value={this.state.descrtemp} />
                  <FormFeedback>Caracter invalido</FormFeedback>
                </FormGroup>
              </Form>
              <Alert color={this.state.colorAlert} isOpen={this.state.alert}>{this.state.messageAlert}</Alert>
                </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.acepProyEdit}>Modificar</Button>
              <Button color="secondary" onClick={this.cancelEdit}>Cancelar</Button>
            </ModalFooter>
          </Modal>
        </div>
       </div>

     );
   }

}
export default Proyect;
