import React from 'react';
import { CardText, Tooltip,

} from 'reactstrap';

class TooltipItem extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.toggleA = this.toggleA.bind(this);
    this.state = {
      tooltipOpen: false,
      tooltipOpenA:false
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen
    });
  }
  toggleA() {
    this.setState({
      tooltipOpenA: !this.state.tooltipOpenA
    });
  }

  name=(namePro)=>{
    console.log("ta,año",namePro);
    if(namePro.length>13){
      return (namePro[0].toUpperCase()+namePro.slice(1, 10))+"..."
    }else{
      return (namePro[0].toUpperCase()+namePro.slice(1,namePro.length))
    }
  }
  descrip=(descPro)=>{
    if(descPro.length>13){
      return (descPro[0].toUpperCase()+descPro.slice(1,23 ))+"..."
    }else{
      return (descPro[0].toUpperCase()+descPro.slice(1,descPro.length))
    }
  }

  render() {
    return (
      <div>
      {/* <CardTitle id={'Tooltip-'+this.props.index}>{this.name(this.props.name)}
      <ButtonGroup className="float-right" style={{"border":"0px"}}>
                   <ButtonDropdown id={`card${this.props.index}`} isOpen={this.state[`card${this.props.index}`] == undefined ? this.state[`card${this.props.index}`]= false :  this.state[`card${this.props.index}`]} toggle={() => { this.toogleSettin(`card${this.props.index}`) }}>
                         <DropdownToggle caret className="p-0 button-dropmenu" color="white" > 
                         <i className="icon-settings animate-trasla "></i>                      
                         </DropdownToggle>
                         <DropdownMenu right>
                           <DropdownItem>
                           <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>delete_outline</i>
                           Eliminar
                           </DropdownItem>
                         </DropdownMenu>
                       </ButtonDropdown>
                     </ButtonGroup>
      </CardTitle> */}
      {/* <Tooltip placement={"top"} isOpen={this.state.tooltipOpen} target={'Tooltip-'+this.props.index} toggle={this.toggle}>
       {this.props.nameC}
       </Tooltip> */}


         <CardText className={"mb-3"}  id={'CardText-'+this.props.index}>{this.descrip(this.props.description)}</CardText>
         <Tooltip placement={"right"} isOpen={this.state.tooltipOpenA} target={'CardText-'+this.props.index} toggle={this.toggleA}>
         {this.props.description[0].toUpperCase()+this.props.description.slice(1,this.props.description.length)}
         </Tooltip>

      </div>
    );
  }
}
export default TooltipItem;
