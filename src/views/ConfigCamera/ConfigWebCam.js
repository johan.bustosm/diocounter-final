/*
Dependencias
*/
import React, {
  Component
} from 'react';
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  Col,
  Alert,
   Modal, ModalHeader, ModalBody, ModalFooter,FormFeedback
} from 'reactstrap';
import {Redirect} from 'react-router-dom'
import request from 'superagent';
/*
Variables de env
*/
import env from "../../env.json";
import "./css/iconosmodal.css";
var device = "webcam";
const expName= /^[a-zA-Z 0-9]*$/;

class ConfigIpCamera extends Component {
  constructor(props) {
    super();
    this.state = {
      id_project: props.project,
      name: "",
      select: " ",
      redir: false,
      image:"",
      validate: false,
      errorname:false,
      expNameV:false,
      alert: false,
      alertV: false,
      modal:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      typeCam:false,
      visibletypeCam:false
    }
    this.toggle = this.toggle.bind(this);
  }
  componentDidMount() {
    this.timerID = setInterval(() => this.requestvalidation(), 500);
  }
  componentWillUnmount() {
    clearInterval(this.timerID);
  }
  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  requestvalidation() {
    if(this.state.alert){
       // console.log("Enviando peticion");
    var user = sessionStorage.getItem("token");
    var joinUser ='token '+user
    request
     .post(env.http+"valispedevices")
     .set('authorization',String(joinUser))
     .send({name:this.state.name,project_id: this.state.id_project})
     .end((err, res) => {
       // console.log(res);
       if(err){
         console.log(err)

       }else{
         if(res.body.code===200){
           this.setState({
             validate:false,
             alert:false,
             modal: true,
             titlemodal:"Usuario",
             bodymodal:"Su cámara ya fue validada",
             iconmodal:"check_circle_outline",
             redir:true
           })

         }else if(res.body.code===203){
           this.setState({
             validate:false,
             alert:false,
             modal: true,
             titlemodal:"Usuario",
             bodymodal:"Ocurrio un error al validar la cámara, valide sus datos.",
             iconmodal:"error_outline"
           })
         }
       }

   });
  }
}
  presave = () => {
    this.setState({
        errorname:false
    })
    console.log(this.state.select);
    if (this.state.id_project === "" || this.state.name === "" || this.state.select === " " ) {
      this.setState({
        modal: true,
        titlemodal:"Usuario",
        bodymodal:"Debe ingresar todos los campos para validar.",
        iconmodal:"warning"
      })
    } else {
      if(this.state.expNameV===false){
      var user = sessionStorage.getItem("token");
      var joinUser = 'token ' + user
      var configure = {
        "type": this.state.select
      }
      request
        .post(env.http + "devices")
        .set('authorization', String(joinUser))
        .send({
          project_id: this.state.id_project,
          name: this.state.name,
          type: device,
          config: configure,
          image: this.state.image
        })
        .end((err, res) => {
          if(err){}else {
            if (res.body.code === 200) {
              this.setState({
                alert: true,
                validate:true,
                errorname:false

              })

            }else if(res.body.code === 401){
              this.setState({
                errorname:true
              })
            }
          }
        });
    }
   }
  }

  alertComputer(){
    if(this.state.errorname){
      return(
        <Alert color="danger">
          Este nombre ya ha sido utilizado previamente para otra cámara en este proyecto.
        </Alert>
      )
    }
  }

  alertValidation(){
    if(this.state.alert){
      return(
        <Alert color="success">
          Por favor termine el proceso de validación de la cámara en su aplicación de escritorio.
        </Alert>
      )

    }
  }

  habdleSelect(event) {

    switch (event.target.value) {
      case "INCORPORADA":
          this.setState({
          select: 0,
          image:"desktop_mac"
        })
        break;
      case "CÁMARA USB":
        this.setState({
          select: 1,
          image:"album"
        })
        break;
      case "USB 1":
        this.setState({
          select: 0,
          image:"album"
        })
        break;
      case "USB 2":
        this.setState({
          select: 1,
          image:"album"
        })
        break;
      default:
        break;
    }
  }
  handleChangeNAME(event) {
    this.setState({
      name: event.target.value
    })
    if(!expName.test(event.target.value)){
      this.setState(
        {
          expNameV :true
        }
      )
    }else{
      this.setState(
        {
          expNameV:false
        }
      )
    }
  }
  imagesIcon(){
    if(this.state.iconmodal==="info"){
      return (<i className="material-icons infomo">info</i>)
    }else if(this.state.iconmodal==="warning"){
      return (<i className="material-icons icowarni">warning</i>)
    }else if(this.state.iconmodal==="check_circle_outline"){
      return (<i className="material-icons icocheck">check_circle_outline</i>)
    }else if(this.state.iconmodal==="error_outline"){
      return (<i className="material-icons iconerror">error_outline</i>)
    }

  }
  typeCamera(){
if(this.state.visibletypeCam){
    if(this.state.typeCam){
      return(
          <div>
          <FormGroup >
            <Label for = "name" > Nombre < /Label>
            <Input maxlength="25" invalid={this.state.expNameV} type = "text" name = "name" id = "name" disabled = {this.state.validate} placeholder = "Nombre del dispotivo" value = {this.state.name} onChange = {this.handleChangeNAME.bind(this)} />
            <FormFeedback>Caracter invalido</FormFeedback>
          </FormGroup>
          {this.alertComputer()}
          <FormGroup >
        <Label for = "exampleSelect" > Cámara < /Label>
        <Input type = "select"  disabled = {this.state.validate} onChange = {this.habdleSelect.bind(this)} name = "select" id = "exampleSelect" >
            <option > < /option>
            <option >INCORPORADA</option>
            <option >CÁMARA USB</option>
        </Input>
        </FormGroup>
        </div>
      )
    }else{
      return(
      <div>
      <FormGroup >
        <Label for = "name" > Nombre < /Label>
        <Input maxlength="25" invalid={this.state.expNameV} type = "text" name = "name" id = "name" disabled = {this.state.validate} placeholder = "Nombre del dispotivo" value = {this.state.name} onChange = {this.handleChangeNAME.bind(this)} />
        <FormFeedback>Caracter invalido</FormFeedback>
      </FormGroup>
      {this.alertComputer()}
      <FormGroup >
      <Label for = "exampleSelect" > Cámara < /Label>
      <Input type = "select"  disabled = {this.state.validate} onChange = {this.habdleSelect.bind(this)} name = "select" id = "exampleSelect" >
          <option > < /option>
          <option >USB 1</option>
          <option >USB 2</option>
      </Input>
      </FormGroup>
      </div>
    )
    }
  }
  }
handleRadio(event){
  console.log(event.currentTarget);
  if(event.currentTarget.value==="si"){
    device = "webcam"
    this.setState({
      typeCam:true,
      visibletypeCam:true
    })
  }else{
    device = "usb"
    this.setState({
      typeCam:false,
      visibletypeCam:true
    })
  }
}
  render() {
    if(this.state.redir && !this.state.modal){
      return(<Redirect to='/camera'/>)
    }
    return (
      <div >
        <Row >
        <Col>
          <Form >
            <h2 className="titleinfoCamera" > Información acerca de su cámara < /h2>
            <Label for = "radio1" > Su equipo cuenta con una cámara webcam integrada < /Label>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="typecam" value="si" onChange={this.handleRadio.bind(this)} /> Si
              </Label>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="typecam" value="no" onChange={this.handleRadio.bind(this)}/> No
              </Label>
            </FormGroup>




            {this.typeCamera()}

            {this.alertValidation()}
              <Row className="mt-5 mb-4">
              <Col >
              </Col>
              <Col className="text-center">
              <Button color="success" size = "lg" onClick = {this.presave}  disabled = {this.state.validate}> Validar < /Button>
              < /Col>
              <Col>
              </Col>
          </Row>
          </Form>
          </Col>
        </Row>
        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}
export default ConfigIpCamera;
