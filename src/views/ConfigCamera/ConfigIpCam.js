/*
Dependencias
*/
import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input,Row, Col,Alert, Modal, ModalHeader, ModalBody, ModalFooter,FormFeedback
} from 'reactstrap';
import request from 'superagent';
import {Redirect} from 'react-router-dom'
/*
Variables de env
*/
import env from "../../env.json"
const device = "ip_camera";
const expIp= /^((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
const expPort= /^[0-9]+$/;
const expName= /^[a-zA-Z 0-9]*$/;
class ConfigIpCamera extends Component {
  constructor(props) {
    super();
    this.state = {
      id_project:props.project,
      name:"",
      ip:"",
      port:"",
      user:"",
      password:"",
      routeRtsp:"",
      redir: false,
      validate: false,
      errorname:false,
      alert: false,
      modal:false,
      expPortV:false,
      expIpV:false,
      expNameV:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      marks:[],
      references:[],
      step:0,
      nameMark:"",
      route:"",
      visibleRef:false
    }
      this.toggle = this.toggle.bind(this);
   }
   componentDidMount() {
     if(this.state.step===0){
       this.loaderMark()
     }
     this.timerID = setInterval(() => this.requestvalidation(), 500);
   }
   componentWillUnmount() {
     clearInterval(this.timerID);
   }
   toggle() {
     this.setState({
       modal: !this.state.modal
     });
   }
   loaderMark(){
     request
       .post(env.http + "allcameras")
       .end((err, res) => {
         if (err) {
           console.log(err)

         } else {
           if (res.status === 200){
             this.setState({
               marks:res.body.message,
               step:1
             })
           }
       }
     })
 }
   requestvalidation() {
     if (this.state.alert) {
       // console.log("Enviando peticion");
       var user = sessionStorage.getItem("token");
       var joinUser = 'token ' + user
       request
         .post(env.http + "valispedevices")
         .set('authorization', String(joinUser))
         .send({
           name: this.state.name,
           project_id: this.state.id_project
         })
         .end((err, res) => {
           // console.log(res);
           if (err) {
             console.log(err)

           } else {
             if (res.body.code === 200) {
               this.setState({
                 validate: false,
                 alert: false,
                 modal: true,
                 titlemodal: "Usuario",
                 bodymodal: "Su cámara ya fue validada",
                 iconmodal: "check_circle_outline",
                 redir: true
               })

             }else if(res.body.code===203){
               this.setState({
                 validate:false,
                 alert:false,
                 modal: true,
                 titlemodal:"Usuario",
                 bodymodal:"Ocurrio un error al validar la cámara, valide sus datos.",
                 iconmodal:"error_outline"
               })
             }
           }

         });
     }
   }
   presave = () => {
     if(this.state.step===3){
       if (this.state.routeRtsp === "" || this.state.routeRtsp === " ") {
         this.setState({
           modal: true,
           titlemodal:"Usuario",
           bodymodal:"Debe ingresar todos los campos para validar.",
           iconmodal:"warning"
         })
       }else {
         console.log("Entre por state 3");
        var user = sessionStorage.getItem("token");
        var joinUser = 'token ' + user
        request
          .post(env.http + "devices")
          .set('authorization', String(joinUser))
          .send({
            project_id: this.state.id_project,
            name: this.state.name,
            type: device,
            config: configure,
            route:this.state.routeRtsp,
            image: "videocam"
          })
          .end((err, res) => {
            console.log(res);
            if (err) {} else {
              if (res.body.code === 200) {
                this.setState({
                  alert: true,
                  validate:true,
                  errorname:false
                })

              } else if (res.body.code === 401) {
                this.setState({
                  errorname: true
                })
              }
            }
          });
      }

     }else{
     if (this.state.ip === "" || this.state.port === "" || this.state.user === "" || this.state.password === "" || this.state.id_project === "" || this.state.name === "") {
       this.setState({
         modal: true,
         titlemodal:"Usuario",
         bodymodal:"Debe ingresar todos los campos para validar.",
         iconmodal:"warning"
       })
     }
    else {

       if(this.state.expNameV===false && this.state.expPortV===false && this.state.expIpV===false){
         var user = sessionStorage.getItem("token");
         var joinUser = 'token ' + user
         var configure = {
           "ip": this.state.ip,
           "port": this.state.port,
           "user": this.state.user,
           "password": this.state.password,
         }
         request
           .post(env.http + "devices")
           .set('authorization', String(joinUser))
           .send({
             project_id: this.state.id_project,
             name: this.state.name,
             type: device,
             config: configure,
             structure: this.state.route,
             image: "videocam"
           })
           .end((err, res) => {
             if (err) {} else {
               if (res.body.code === 200) {
                 this.setState({
                   alert: true,
                   validate:true,
                   errorname:false
                 })

               } else if (res.body.code === 401) {
                 this.setState({
                   errorname: true
                 })
               }
             }
           });

       }

     }
   }
   }

   alertComputer() {
     if (this.state.errorname) {
       return (
         <Alert color = "danger" >
         El nombre de la cámara ya fue registrado para este proyecto, debe cambiarlo.
         </Alert>
       )
     }
   }
   alertValidation(){
     if(this.state.alert){
       return(
         <Alert color="success">
           Por favor termine el proceso de validación de la cámara en su aplicación de escritorio.
         </Alert>
       )

     }
   }

   imagesIcon() {
       if (this.state.iconmodal === "info") {
         return ( < i className = "material-icons infomo" > info < /i>)
         }
         else if (this.state.iconmodal === "warning") {
           return ( < i className = "material-icons icowarni" > warning < /i>)
           }
           else if (this.state.iconmodal === "check_circle_outline") {
             return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
             }else if(this.state.iconmodal==="error_outline"){
               return (<i className="material-icons iconerror">error_outline</i>)
             }
           }
handleChangeNAME(event){this.setState({     name:event.target.value})
if(!expName.test(event.target.value)){
  this.setState(
    {
      expNameV :true
    }
  )
}else{
  this.setState(
    {
      expNameV:false
    }
  )
}
}
handleChangeIP(event){this.setState({     ip:event.target.value})
if(expIp.test(event.target.value)===false){
  this.setState(
    {
      expIpV :true
    }
  )
}else{
  this.setState(
    {
      expIpV:false
    }
  )
}
}
handleChangePORT(event){
  this.setState({   port:event.target.value}
  )
  if(expPort.test(event.target.value)===false){
    this.setState(
      {
        expPortV :true
      }
    )
  }else{
    this.setState(
      {
        expPortV:false
      }
    )
  }
}
handleChangeUSER(event){this.setState({   user:event.target.value})}
handleChangePASS(event){this.setState({   password:event.target.value})}
handleChangerouterRtsp(event){this.setState({   routeRtsp:event.target.value})}

handleChangeRefer(event){
  console.log(event.target.value);
  if(event.target.value==="none"){
    this.setState({
      step:3
    })
  }else if(event.target.value!==""){
    this.setState({
      route:event.target.value,
      step:4
    })
  }else{
    this.setState({
      step:2
    })
  }
}
handleChangeMark(event){

  if(event.target.value==="Otra"){
    this.setState({
      step:3
    })
  }else if(event.target.value!==""){
    this.setState({
      nameMark:event.target.value,
      step:1.5
    })
  }else{
    this.setState({
      visibleRef:false,
      step:1
    })
  }
}
loaderForm(){
  if(this.state.step===4){
    return(
      <div>
            <FormGroup>
                           <Label for="name">Nombre</Label>
                           <Input maxlength="25" invalid={this.state.expNameV}  type="text" name="name" id="name" disabled = {this.state.validate}  placeholder="Nombre del dispotivo"  value={this.state.name} onChange={this.handleChangeNAME.bind(this)} />
                           <FormFeedback>Caracter invalido</FormFeedback>

                           {this.alertComputer()}
                         </FormGroup>
                          <FormGroup>
                            <Label for="ip">IP</Label>
                            <Input invalid={this.state.expIpV} type="text" name="ip" id="ip" disabled = {this.state.validate} placeholder="192.168.1.1"  value={this.state.ip} onChange={this.handleChangeIP.bind(this)} />
                             <FormFeedback>No es una dirección Ipv4 </FormFeedback>
                          </FormGroup>
                          <FormGroup>
                            <Label for="port">Puerto</Label>
                            <Input maxlength="6" invalid={this.state.expPortV} type="text" name="port" id="port"disabled = {this.state.validate}  placeholder="554" value={this.state.port} onChange={this.handleChangePORT.bind(this)}/>
                            <FormFeedback>Caracter invalido</FormFeedback>
                          </FormGroup>
                          <FormGroup>
                            <Label for="User">Usario</Label>
                            <Input type="text" name="user" id="user" disabled = {this.state.validate}  placeholder="admin" value={this.state.user} onChange={this.handleChangeUSER.bind(this)}/>
                          </FormGroup>
                          <FormGroup>
                            <Label for="examplePassword">Constraseña</Label>
                            <Input type="password" name="password" disabled = {this.state.validate} id="examplePassword" placeholder="contraseña " value={this.state.password} onChange={this.handleChangePASS.bind(this)} />
                          </FormGroup>
                          {this.alertValidation()}
                          <Row className="mt-5 ">
                          <Col ></Col>
                          <Col className="text-center">
                          <Button  color="success" size = "lg" onClick = {this.presave} className = "mb-4" disabled = {this.state.validate}> Validar < /Button>
                          </Col>
                          <Col></Col>
                          </Row>
                        </div>
                        )
  }else if(this.state.step===3){
    return(
      <div>
      <FormGroup>
      <Label for="name">Nombre</Label>
      <Input maxlength="25" invalid={this.state.expNameV}  type="text" name="name" id="name" disabled = {this.state.validate}  placeholder="Nombre del dispotivo"  value={this.state.name} onChange={this.handleChangeNAME.bind(this)} />
      <FormFeedback>Caracter invalido</FormFeedback>
      <Label for="name">Ingrese la ruta RTSP de su cámara</Label>
      <Input maxlength="100" invalid={this.state.expNameV}  type="text" name="name" id="name" disabled = {this.state.validate}  placeholder="Ruta rtsp"  value={this.state.routeRtsp} onChange={this.handleChangerouterRtsp.bind(this)} />
      <FormFeedback>Caracter invalido</FormFeedback>
      </FormGroup>
      {this.alertValidation()}
      <Row className="mt-5 ">
      <Col ></Col>
      <Col className="text-center">
      <Button  color="success" size = "lg" onClick = {this.presave} className = "mb-4" disabled = {this.state.validate}> Validar < /Button>
      </Col>
      <Col></Col>
      </Row>
      </div>
    )
  }else if(this.state.step===1.5){
    request
      .post(env.http + "allref")
      .send({mark:this.state.nameMark})
      .end((err, res) => {
        if (err) {
          console.log(err)

        } else {
          if (res.status === 200){
            this.setState({
              references:res.body.message,
              step:2,
              visibleRef:true
            })
          }
      }
  })
}
}
loaderRefere(){
  if(this.state.visibleRef){
    var referens = this.state.references.map((obj,index)=>{
      return(
        <option key={index} value={obj.rtsp}>{obj.name}</option>
      )
    })

    return(
      <div>
      <FormGroup >
      <Label for = "exampleSelect" > Referencia < /Label>
      <Input type = "select"  disabled = {this.state.validate} onChange={this.handleChangeRefer.bind(this)} name = "select" id = "exampleSelect" >
      {referens}
      </Input>
      </FormGroup>
      </div>
    )
  }
}
render() {
  if(this.state.redir && !this.state.modal){
    return(<Redirect to='/camera'/>)
  }

  var camarasI = this.state.marks.map((obj,index)=>{
    return(
      <option key={index}>{obj.mark}</option>
    )
  })

    return (
              <div>
                <Row>
                  <Col>
                   <Form>
                     <h2 className="titleinfoCamera">Información acerca de su cámara </h2>

                      <FormGroup >
                      <Label for = "exampleSelect" > Marca < /Label>
                      <Input type = "select"  disabled = {this.state.validate} onChange={this.handleChangeMark.bind(this)} name = "select" id = "exampleSelect" >
                          {camarasI}
                      </Input>
                      </FormGroup>
                      {this.loaderRefere()}
                      {this.loaderForm()}
                    </Form>
                      </Col>
                </Row>
                <div>
                  <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                    <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
                    <ModalBody>
                      <Row>
                      <Col className="md-2 p-0 text-aling text-center">
                      {this.imagesIcon()}
                      </Col>
                      <Col className="md-10 p-0 mt-4 text-aling text-left">
                        <p className="bodymessa">{this.state.bodymodal}</p>
                      </Col>
                      </Row>
                    </ModalBody>
                    <ModalFooter>
                      <Button color="primary" onClick={this.toggle}>Aceptar</Button>
                    </ModalFooter>
                  </Modal>
                </div>
          </div>
    );
   }
}
export default ConfigIpCamera;
