/*
Dependencias
*/
import React, { Component } from 'react';
import io from 'socket.io-client';
import Canvas from './components/canvas'
import { Button, Form, FormGroup, Label, Input, FormText, Row, Col,CustomInput } from 'reactstrap';

/*
Estilos
*/
import "./phone.css"

/*
Variables de env
*/
import env from "../../env.json"
const device = "ip_camera"

//const addresIpSocket = "192.168.1.114:4000";
//const device = "ip_camera"

// var counteven=0
class ConfigIpCamera extends Component {
  constructor() {
    super();
    this.state = {
      image_src:"photo-camera.png",
      cleanImage:true,
      sendMessage:true,
      visible:false,
      ip:"",
      port:"",
      user:"",
      password:"",
      state_conexion:"red",
      checkedList:[false,false,false,false,false],
      select:""
    };
   }
  componentDidMount() {
    /*
    Iniciando comunicacion por medio de sockets
    */
    this.clienteSocketFrom = io(env.addresSocketFront, { forceNew: true });
    this.clienteSocketCam = io(env.addresSocketCam, { forceNew: true });
    /*
    Escuchando eventos provenientes de la sala From
    */
    this.clienteSocketFrom.on("image", (data) => {
        console.log(data.buffer)
       this.setState({
         image_src:"data:image/jpeg;base64,"+String(data.buffer)
       });
      });
    /*
    Escuchando eventos provenientes de la sala Cam
    */
    this.clienteSocketCam.on("state", (data) => {
       this.setState({
             state_conexion:data
       });
    });

  }
  pedirCam =()=>{
    // Enviando mensaje para solicitar la vista de la cam
    console.log("despuesdel emit")
    var messages = {
      "key": "take",
      "messages": {
        "device": device,
        'ip': this.state.ip,
        'port': this.state.port,
        'user': this.state.user,
        'password': this.state.password
       }
      }
    this.clienteSocketCam.emit("picture", messages)

  }
  send =()=>{
      console.log("Estoy cambiando de esto en send")
      this.setState({
      sendMessage: !this.state.sendMessage
    })
  }
  clean =()=>{
      this.setState({
     cleanImage: !this.state.cleanImage
    })

  }

  vali =()=>{

      if(this.state.ip!="" && this.state.port!="" && this.state.user!="" && this.state.password!="" ){
        this.setState({
          visible:true
        })
        var message = {
          'key': 'validar',
          'messages': {
            'device': device,
            'ip': this.state.ip,
            'port': this.state.port,
            'user': this.state.user,
            'password': this.state.password,
            'category':this.state.checkedList
           }
        }
        this.clienteSocketCam.emit("validation", message)
      }else{
        alert("Usuario recuerde llenar todos los campos")
      }
  }

  circulo=()=>{
    if(this.state.state_conexion==="red"){
      return(<div className="circuloR"></div>)
    }else if(this.state.state_conexion==="orange"){
      return (<div className="circuloO"></div>)
    } else if (this.state.state_conexion==="green"){
      return(<div className="circuloG"></div>)
    }else if (this.state.state_conexion==="blue"){
      return (<div><div className="circuloA"></div><p>Good</p></div>)
    }
  }

  mostrar=()=>{
    if(this.state.visible){
      return (
      <div>
      <Col md="10">
      <Row className="mb-4 text-aling text-center">

              <Col><Canvas url_image={this.state.image_src} type={this.state.select} category={this.state.checkedList} clean={this.state.cleanImage} send={this.state.sendMessage} /></Col>

      </Row>
      <Row >



              <Col md="4">
                < Button className="ml-3 mr-3" color="primary" size="lg" onClick={this.pedirCam} size="lg" block><i className="material-icons ">camera_enhance</i>Take</Button>
              </Col>
              <Col md="4">
                < Button className="ml-3 mr-3" color="primary" size="lg" onClick={this.clean} size="lg" block><i className="material-icons ">delete_forever</i>Clean</Button>
              </Col>
                <Col md="4">
                < Button className="ml-3" color="primary" size="lg" onClick={this.send} size="lg" block><i className="material-icons">send</i>Send</Button>
              </Col>



        </Row>
        </Col>
      </div>
        )
    }
  }
handleChecked=(event)=>{
  var temp = this.state.checkedList
  switch (event.target.id) {
    case "1":
       temp[0]=event.target.checked;
       this.setState({checkedList:temp});
        break;
    case "2":
       temp[1]=event.target.checked;
       this.setState({checkedList:temp});
        break;
    case "3":
        temp[2]=event.target.checked;
        this.setState({checkedList:temp});
        break;
    case "4":
        temp[3]=event.target.checked;
        this.setState({checkedList:temp});
        break;
    case "5":
        temp[4]=event.target.checked;
        this.setState({checkedList:temp});
        break;
    case "6":
        temp[5]=event.target.checked
        this.setState({checkedList:temp});
        break;
    default:
       break;
}

   console.log(this.state.checkedList)
}
handleChangeIP(event){this.setState({     ip:event.target.value})}
handleChangePORT(event){this.setState({   port:event.target.value})}
handleChangeUSER(event){this.setState({   user:event.target.value})}
handleChangePASS(event){this.setState({   password:event.target.value})}
habdleSelect(event){
  this.setState({select:1})
  switch (event.target.value) {
    case "Counting":
       this.setState({select:1})
       break;
    case "Detection":
        this.setState({select:0})
        break;
    default:
           break;
    }

  // console.log(event.target.value)
}
  render() {
    console.log(this.state.select)
    return (


              <div>
                <Row>
                  <Col></Col>
                  <Col>  <h1 className="text-center">Settings Ip Camera</h1> </Col>
                  <Col></Col>
                </Row>
                <Row>
                  <Col md="2">
                   <Form>
                     <h2>Information about cameras</h2>
                      <FormGroup>
                        <Label for="ip">IP</Label>
                        <Input type="text" name="ip" id="ip" placeholder="192.168.1.1"  value={this.state.ip} onChange={this.handleChangeIP.bind(this)} />
                      </FormGroup>
                      <FormGroup>
                        <Label for="port">Port</Label>
                        <Input type="text" name="port" id="port" placeholder="554" value={this.state.port} onChange={this.handleChangePORT.bind(this)}/>
                      </FormGroup>
                      <FormGroup>
                        <Label for="User">User</Label>
                        <Input type="text" name="user" id="user" placeholder="admin" value={this.state.user} onChange={this.handleChangeUSER.bind(this)}/>
                      </FormGroup>
                      <FormGroup>
                        <Label for="examplePassword">Password</Label>
                <Input type="password" name="password" id="examplePassword" placeholder="password " value={this.state.password} onChange={this.handleChangePASS.bind(this)} />
                      </FormGroup>
                        <FormGroup>
                        <Label for="exampleSelect">Select</Label>
                        <Input type="select"   onClick={this.habdleSelect.bind(this)} name="select" id="exampleSelect">
                        <option></option>
                        <option>Counting</option>
                        <option>Detection</option>
                        </Input>
                        </FormGroup>
                        <FormGroup>
                          <Label for="exampleCheckbox">Category Detection</Label>
                          <div>
                            <CustomInput type="checkbox" id="1" onChange={this.handleChecked.bind(this)} label="Human" />
                            <CustomInput type="checkbox" id="2" onChange={this.handleChecked.bind(this)}  onChange={this.handleChecked.bind(this)}label="Knife" />
                            <CustomInput type="checkbox" id="3" onChange={this.handleChecked.bind(this)} label="Bottle" />
                            <CustomInput type="checkbox" id="4" onChange={this.handleChecked.bind(this)} label="Car" />
                            <CustomInput type="checkbox" id="5" onChange={this.handleChecked.bind(this)} label="Dog" />
                            <CustomInput type="checkbox" id="6" onChange={this.handleChecked.bind(this)} label="Airplane" disabled />
                          </div>
                        </FormGroup>
              <Row>
                <Col>
              <Button className="mb-4"onClick={this.vali} >Validate</Button>
                  </Col>
               <Col>
                  {this.circulo()}
               </Col>
               </Row>
                    </Form>
                  </Col>
          <Col md="10">
            {this.mostrar()}
          </Col>
                </Row>
          </div>
    );
   }
}
export default ConfigIpCamera;
