import React, { Component } from "react";
import "./cardbody.css"
import ProgressBar from "../progressBar";

class CardBody extends Component{
     render(){
        return <div className="col text-center">
            <div className="card-data data">
              <div className="card-header header">
                {/* <div className="circulo"/> */}
                <h2>{this.props.categoria} </h2>
                {/*<h2> puerta {this.props.data.puerta} </h2>*/}
              </div>
              <div className="card-body body">
                <h2>Capacidad:{this.props.data.capacidad}</h2>
                <h2>Ingresaron:</h2>
                <h3>{this.props.data.cantidad}</h3>
                <div>
                  <ProgressBar visible={this.props.visible} porcentaje={this.props.porcentaje} statePbar={this.props.stateProgressBar} theme={this.props.theme} />
                </div>
              </div>
            </div>
          </div>;
    }
};

export default CardBody;
