import {Progress} from "react-sweet-progress";
import "react-sweet-progress/lib/style.css";
import React, {Component} from 'react';
class ProgressBar extends Component {
  // constructor() {
  //   super();
  // }
  validarRender() {
    if (this.props.visible) {
      return (
        <div className = "progress" >
          <Progress percent = {this.props.porcentaje} status = {this.props.statePbar} theme = {this.props.theme}/>
        </div >
      )
    }
  }

  render() {
    return (
      <div>
        {this.validarRender()}
      </div>
    );
  }
};

export default ProgressBar;
