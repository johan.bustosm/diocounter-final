import React, {Component} from "react";
import  "./card-info.css"

class CardInfo extends Component{

    render(){
        return <div className="card-info">
            <div className="container">
              <div className="row">
                <div className="col-6">
                  <h1>Evento : Reunion - culto </h1>
                  <h1>Ciudad : Bogotá D.C </h1>
                  <h1>Dirección : av suba con calle 100 </h1>
                </div>
                <div className="col-6">
                  <h2>Horario : 3:00 pm - 5:00 pm </h2>
                  <h2>Fecha : 24 de abril de 2018 </h2>
                  <h2>Invitados : Ninguno </h2>
                </div>
              </div>
            </div>
          </div>
        

    };

}

export default CardInfo;

