//import "./app.css";
import React, {
  Component
} from 'react';
import io from 'socket.io-client';
import CardBody from './components/cardbody'
import ProgressBar from './components/progressBar'
import env from "../../env.json"


class Counter extends Component {
  constructor() {
    super();
    this.state = {
      messages: [{
          puerta: "",
          cantidad: "-",
          capacidad: ""
        },
        {
          puerta: "",
          cantidad: "-",
          capacidad: ""
        },
        {
          puerta: "",
          cantidad: "-",
          capacidad: ""
        }
      ],
      buttonPause: true,
      reset: false,
      visibleProgressbar: false,
      porcentaje: 0,
      stateProgressBar: "active",
      theme: {
        active: {
          color: "#388312"
        }
      }
    };
    this.onclear = this.onc
  }

  componentDidMount() {
    this.clienteSocket = io(env.addresSocketFront, {
      forceNew: true
    });
    //cuando este cliente tenga un evento con message va actualizar el estado de message
    this.clienteSocket.on("message", message => {
      this.setState({
        messages: message

      });

    });
    this.clienteSocket.on("ocuPorcentaje", porcentaje => {
      if (this.state.visibleProgressbar === false) {
        if (porcentaje > 0) {
          this.setState({
            visibleProgressbar: true
          });
        }

      }
      console.log(porcentaje)
      if (porcentaje < 60) {
        this.setState({
          porcentaje,
          theme: {
            active: {
              color: "#388312"
            }
          }
        });
      } else if (porcentaje >= 60 && porcentaje <= 90) {
        this.setState({
          porcentaje,
          theme: {
            active: {
              color: "#e19613"
            }
          }
        });
      } else if (porcentaje >= 90 && porcentaje <= 100) {
        this.setState({
          porcentaje,
          stateProgressBar: "active",
          theme: {
            active: {
              color: "#e11f13"
            }
          }
        });

      }

    });
  }

  ClearCount = () => {
    // this.setState((prevState)=>{
    //    reset: !prevState.reset;
    // })

    // this.camareSocket2 = io("http://" + addresIpSocket + "/cameras", {
    this.camareSocket2 = io(env.addresSocketCam, {
      forceNew: true
    });
    this.camareSocket2.emit("reset", "all")
    this.setState({
      visibleProgressbar: false
    });
    // request
    // .get("http://" + addresIpSocket+"/reset")
    // .then((res)=>{
    // });
  }

  render() {

    const readDatos = this.state.messages.map((obj, index) => {


      return <CardBody data = {
        obj
      }
      key = {
        index
      }
      visible = {
        this.state.visibleProgressbar
      }
      porcentaje = {
        obj.porcentaje
      }
      categoria = {
        obj.cat
      }
      statePbar = {
        "active"
      }
      theme = {
        this.state.theme
      }
      />;
    });

    return <div >


      <
      div className = "container" >
      <
      div className = "row " >
      <
      div className = "col" >
      <
      h1 className = "text-center mt-4" > Contador < /h1> <
      /div> <
      /div> {
        /* <div className="row">
                  <div className="col">
                    <CardInfo />
                  </div>
                </div> */
      } <
      div className = "row mt-4" > {
        readDatos
      } < /div> <
      div className = "row mt-6" >
      <
      div className = "col text-center mt-4 " >
      <
      h3 > Porcentaje total de ocupación < /h3> <
      ProgressBar visible = {
        this.state.visibleProgressbar
      }
    porcentaje = {
      this.state.porcentaje
    }
    statePbar = {
      this.state.stateProgressBar
    }
    theme = {
      this.state.theme
    }
    /> <
    /div> <
    /div> {
      /* <div className="row mt-4">
                <div className="col text-center ">
                       <h2> Porcentaje de ocupación total </h2>
                </div>
              </div> */
    } <
    div className = "row mt-4" >
      <
      div className = "col text-center " >
      <
      button type = "button "
    className = "btn btn-primary"
    onClick = {
        this.ClearCount
      } >
      Reiniciar conteo <
      /button> <
      /div> <
      /div> <
      /div> <
      /div>;



  }
}

export default Counter;
