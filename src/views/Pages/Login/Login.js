import React, { Component } from 'react';
import { Button, Card, CardBody, CardGroup, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {Link , Redirect} from 'react-router-dom'
import request from 'superagent'
import logo from '../../../assets/img/brand/logo.svg'
import navigation from '../../../_nav2.js';
import store from '../../../store'
import env from "../../../env.json"
import "./login.css"
class Login extends Component {
  constructor() {
    super();
    this.state = {
      email:"",
      password:"",
      login:false,
      token:"",
      modal:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
    };
    this.toggle = this.toggle.bind(this);
   }

   /*
   Capturando la informacion de los elementos input
   */
    handleChangeValiPass (event){
        if(event.key == 'Enter'){
          this.singIn()
        }
    } 
   handleChangeEmail(event){this.setState({   email:event.target.value})}
   handleChangePass(event){this.setState({   password:event.target.value})}
   /*
   Realizar la solitud para SingIn
   */
   singIn=()=>{
         if(this.state.email==="" || this.state.password===""){
           this.setState((prevState, props) => {
           return ({
               modal:true,
               titlemodal:"Campos vacios",
               bodymodal:"Porfavor complete todos los campos.",
               iconmodal:"info"
             });
           });
       //alert("Señor Usuario por favor llenar todos los campos")
     }else{
       request
        .post(env.http+"singin")
        .send({email: this.state.email , password: this.state.password, type:"web" }) // sends a JSON post body
        .end((err, res) => {
          if(res===undefined){
            // console.log("Error",err)
            // console.log("Respuesta de la peticion",res)
            this.setState((prevState, props) => {
              return ({
                modal:true,
                titlemodal:"Error en el servicio",
                bodymodal:"Porfavor contactar con su administrador.",
                iconmodal:"warning"
              });
            });
          }else{
            // console.log(res.status);
            if(res.status===200){
              store.dispatch({
                type:"SING_IN",
                token:res.body.token,
                menu:navigation
              })
              sessionStorage.setItem("menu",navigation)
              sessionStorage.setItem("token",res.body.token)
              sessionStorage.setItem("name",res.body.name)
              sessionStorage.setItem("first",res.body.first)
              sessionStorage.setItem("last",res.body.last)


              sessionStorage.setItem("email",this.state.email)

              this.setState({
                login:true
              })
            }else if(res.status===404 || res.status===501 ){
              this.setState((prevState, props) => {
                return ({
                  modal:true,
                  titlemodal:"Usuario o contraseña incorrecta",
                  bodymodal:"Señor Usuario por favor revise los datos ingresados al sistema",
                  iconmodal:"info"
                });
              });
              // alert("Usuario o contraseña incorrecta")
            }else if(res.status===401){
              this.setState((prevState, props) => {
                return ({
                  modal:true,
                  titlemodal:"Usuario",
                  bodymodal:"Recuerde que debe validar primero su cuenta.",
                  iconmodal:"info"
                });
              });
            }

          }
      });
     }
   }
   toggle() {
     this.setState({
       modal: !this.state.modal
     });
   }
   // imagesIcon(){
   //   if(this.state.iconmodal){
   //
   //     return (<i className="material-icons icologinche">check_circle_outline</i>)
   //   }else{
   //     return (<i className="material-icons icologinerr">error_outline</i>)
   //   }
   // }
   imagesIcon(){
     if(this.state.iconmodal==="info"){
       return (<i className="material-icons infomologin">info</i>)
     }else if(this.state.iconmodal==="warning"){
       return (<i className="material-icons icowarnilogin">warning</i>)
     }else if(this.state.iconmodal==="check_circle_outline"){
       return (<i className="material-icons icocheck">check_circle_outline</i>)
     }
   }
  render() {
    if(this.state.login){
      return(<Redirect to='/project'/>)
    }
    if(this.state.demo){
      return(<Redirect to='https://globai.co/contactenos/'/>)
    }
    return (
      <div id="PrimaryLogin">
        
        <div id="contLogin">

         <div id="topLogin">
         <img  src="./agua.svg"  alt="agua efecto" />
       </div>
       
        <div id="boxLogin">
       <img  src="./loginLogo.svg"  id="imaLogo" alt="Logo Intelligex" />
       
       </div>
       <span>
       ¡ Empoderando en la 4RI ! 
      </span>
      <div id="contbutton"> 
      <Button outline onClick={()=>{
          window.open("https://globai.co/contactenos/");
      }}>Solicitar Demo</Button>
      </div> 
       <div id="buttLogin">
       <img  src="./agua.svg"  alt="agua efecto" />
       </div > 
       </div >
       
       <div id="formLogin">
       
       <div id="ImagenLogin">
       <img  src="./logointelligexLogin.png"  alt="logintelligex" />
       </div > 
       <h1>Iniciar sesión</h1>
       <p className="text-muted"></p>
                    <InputGroup className="mb-3">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-user"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="text" placeholder="Correo" value={this.state.email} onChange={this.handleChangeEmail.bind(this)} />
                    </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password" placeholder="Contraseña" value={this.state.password} onKeyPress={this.handleChangeValiPass.bind(this)} onChange={this.handleChangePass.bind(this)} />
                    </InputGroup>

       

                    <Row>
                      <Col  className="text-center">
                        <Button color="primary"  onClick={this.singIn}>Ingresar</Button>
                      </Col>
                    </Row>
                    <Row>  
                      <Col  className="text-center">
                          <Link to="/forget">¿Olvide contraseña?</Link>
                      </Col>
                    </Row>
      </div>
      <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className=" text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="mt-4 text-aling text-center">
                <p>{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    );
  }
}

export default Login;
