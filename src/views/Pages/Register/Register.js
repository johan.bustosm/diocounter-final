import React, { Component } from 'react';
import { Button, Card, CardBody, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalHeader, ModalBody, ModalFooter, FormFeedback } from 'reactstrap';
import request from 'superagent';
import Recaptcha from 'react-recaptcha';
import logo from '../../../assets/img/brand/logo.svg'
import env from "../../../env.json"
import "./regis.css"
const expName = /^[a-zA-Z ]*$/;
const expEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
const expPass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/
class Register extends Component {
  constructor() {
    super();
    this.state = {
      first:"",
      last:"",
      email:"",
      password:"",
      passwordR:"",
      transition:false,
      create:false,
      modal:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      expPassV:false,
      expLastV:false,
      expEmailV:false,
      expNameV:false,
      expPasV:false,
      recaptchaV:true
    };
    this.toggle = this.toggle.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
   }
   handleChangeFirst(event){
     if(this.state.first.length>0){

       this.setState({first:event.target.value})
     }else{
       this.setState({first:event.target.value.toUpperCase()})
     }
   }
   handleChangeLast(event){
     if(this.state.last.length>0){

       this.setState({last:event.target.value})
     }else{
       this.setState({last:event.target.value.toUpperCase()})
     }
   }
   handleChangeEmail(event){this.setState({email:event.target.value})}
   handleChangePass(event){this.setState({password:event.target.value})
   if(expPass.test(event.target.value)===false){
     this.setState(
       {
         expPasV :true
       }
     )
   }else{
     this.setState(
       {
         expPasV:false
       }
     )
   }
 }
   handleChangePassRe(event){this.setState({   passwordR:event.target.value}) }

   createuser=()=>{
    if(this.state.first==="" || this.state.last==="" || this.state.email==="" || this.state.password===""){
      this.setState((prevState, props) => {
      return ({
          modal:true,
          titlemodal:"Campos vacios",
          bodymodal:"Debe completar todos los campos",
          iconmodal:false
        });
      });
    }else{
      var count = 0
      if(expName.test(this.state.first)===false){
        this.setState(
          {
            expNameV :true
          }
        )
        console.log("nombre");
        count = count +1
      }else{
        this.setState(
          {
            expNameV:false
          }
        )
      }

      if(expName.test(this.state.last)===false){
        this.setState(
          {
            expLastV :true
          }
        )
        console.log("apellido");
        count = count +1
      }else{
        this.setState(
          {
            expLastV:false
          }
        )
      }

      if(expEmail.test(this.state.email)===false){
        this.setState(
          {
            expEmailV :true
          }
        )
        console.log("email");
        count = count +1
      }else{
        this.setState(
          {
            expEmailV:false
          }
        )
      }

      if(this.state.passwordR===this.state.password){
        this.setState(
          {
            expPassV :false
          }
          )
      }else{
        this.setState(
          {
            expPassV:true
          }
        )
        console.log("password");
        count = count +1
      }

       if(count>=1){
         return true
       }else{
         request
         .post(env.http+"singup")
         .send({first: this.state.first,  last: this.state.last , email: this.state.email , password: this.state.password }) // sends a JSON post body
         .end((err, res) => {
           if(res===undefined){
             this.setState((prevState, props) => {
               return ({
                 modal:true,
                 titlemodal:"Error del servidor",
                 bodymodal:"Contacte con el administrador del servicio",
                 iconmodal:false
               });
             });
           }else{
             console.log(res)
             if(res.status===200){
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"Registro completado",
                   bodymodal:"Gracias por ser parte de nosotros, recuerda que tienes que validar tu cuenta por medio de tu correo electrónico.",
                   iconmodal:true,
                   transition:true
                 });
               });
             }else{
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"El usuario ya existe",
                   bodymodal:"El correo electrónico ya fue registrado",
                   iconmodal:false
                 });
               });
             }
           }
         });
       }
    }
   }
   toggle() {
     this.setState({
       modal: !this.state.modal
     });
     if(this.state.transition){
       this.setState({
         create: true
       });
     }
   }
   imagesIcon(){
     if(this.state.iconmodal){
       return (<i className="material-icons iconcheckregis">check_circle_outline</i>)
     }else{
       return (<i className="material-icons iconmodaleregis">error_outline</i>)
     }
   }
   verifyCallback(response){
     if(response){
       this.setState({
         recaptchaV:false
       })
     }
   }
  render() {
    if(this.state.create){

      return(
        <div className="app flex-row align-items-center mt-5 mb-5">

        <Container>

          <Row className="justify-content-center">

            <Col md="6">
              <Card className=" cardindustrialWelcom">

                <CardBody >
                <Row>
                <Col className="text-center">
                <img id = "user" src={logo} className="text-center" alt="Logo Intelligex" />
                </Col>
                </Row>
                  <h1 className="text-center mb-3">¡Bienvenido a Intelligex!</h1>
                  <p className="mb-1">Intelligex te da la bienvenida <b>{this.state.first+" "+this.state.last}</b>,</p>
                  <p>Para finalizar el proceso de registro continua con los siguientes pasos :</p>
                  <ul>
                  <li>Debes validar primero tu cuenta por medio del correo electrónico.</li>
                  <li>Una vez validada tu cuenta, ya puedes ingresar y disfrutar de todas las aplicaciones a tu disposición.</li>
                  </ul>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
        </div>
      )
    }
    return (
      <div >

        <Container>

          <Row className="justify-content-center">

            <Col md="6" className="mt-4">
            
              <Card className="cardindustrial">
       
                <CardBody>
                <Row>
                <Col className="text-center">
                <img id = "user" src={logo} className="text-center logoImaRegister " alt="Logo Intelligex" />
                </Col>
                </Row>
                  <h1>Registrate</h1>
                  <p className="text-muted">Crea tu cuenta</p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" invalid={this.state.expNameV} placeholder="Nombre" value={this.state.first} onChange={this.handleChangeFirst.bind(this)} />
                    <FormFeedback>Caracter invalido</FormFeedback>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-user"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" invalid={this.state.expLastV} placeholder="Apellido" value={this.state.last} onChange={this.handleChangeLast.bind(this)} />
                    <FormFeedback>Caracter invalido</FormFeedback>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" invalid={this.state.expEmailV} placeholder="Correo electrónico" value={this.state.email} onChange={this.handleChangeEmail.bind(this)}  />
                    <FormFeedback>Correo electrónico invalido</FormFeedback>
                  </InputGroup>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input  type="password" onCopy="return false" invalid={this.state.expPasV}  placeholder="Contraseña" value={this.state.password} onChange={this.handleChangePass.bind(this)}  />
                    <FormFeedback>La contraseña debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.</FormFeedback>
                  </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password"  onpaste="return false"  invalid={this.state.expPassV} placeholder="Repetir contraseña" value={this.state.passwordR} onChange={this.handleChangePassRe.bind(this)} />
                      <FormFeedback>No coinciden las contraseñas</FormFeedback>
                    </InputGroup>
                    <Row className="text-center">
                    <Col className="md-1"></Col>
                    <Col className="md-10 text-center mr-3">
                      <Recaptcha
                      sitekey="6Ld253MUAAAAAOqmeU-Lvb-0730opcyOg5Bbg4ci"
                      type="image"
                      verifyCallback={this.verifyCallback}
                      hl="es"
                      />
                    </Col>
                    <Col  className="md-1"></Col>
                    </Row>
                  <Button className="mt-3" onClick={this.createuser} color="success" block>Crear cuenta</Button>
                  </CardBody>
                {
                //   <CardFooter className="p-4">
                //   <Row>
                //     <Col xs="12" sm="6">
                //       <Button className="btn-facebook" block><span>facebook</span></Button>
                //     </Col>
                //     <Col xs="12" sm="6">
                //       <Button className="btn-twitter" block><span>twitter</span></Button>
                //     </Col>
                //   </Row>
                // </CardFooter>
              }
              </Card>
            </Col>
          </Row>
        </Container>
        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className=" text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="mt-4 text-aling text-center">
                <p>{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>

    );
  }
}

export default Register;
