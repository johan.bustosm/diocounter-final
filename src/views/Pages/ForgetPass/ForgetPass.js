import React, { Component } from 'react';
import { Button, Card, CardBody, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalHeader, ModalBody, ModalFooter, FormFeedback } from 'reactstrap';
import request from 'superagent';
import logo from '../../../assets/img/brand/logo.svg'
import env from "../../../env.json"
import "./regis.css"
const expEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
class ForgetPass extends Component {
  constructor() {
    super();
    this.state = {
      email:"",
      modal:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      expEmailV:false,
      send:false
    };
    this.toggle = this.toggle.bind(this);
   }

   handleChangeEmail(event){this.setState({email:event.target.value})}
   sendEmail=()=>{
    if(this.state.email===""){
      this.setState((prevState, props) => {
      return ({
          modal:true,
          titlemodal:"Campos vacios",
          bodymodal:"Debe completar todos los campos",
          iconmodal:false
        });
      });
    }else{
      var count = 0
      if(expEmail.test(this.state.email)===false){
        this.setState(
          {
            expEmailV :true
          }
        )
        console.log("email");
        count = count +1
      }else{
        this.setState(
          {
            expEmailV:false
          }
        )
      }

       if(count>=1){
         return true
       }else{
         request
         .post(env.http+"changepassemail")
         .send({email: this.state.email })
         .end((err, res) => {
           if(res===undefined || res.status===500){
             this.setState((prevState, props) => {
               return ({
                 modal:true,
                 titlemodal:"Error en el servidor",
                 bodymodal:"Por favor intente más tarde.",
                 iconmodal:false
               });
             });
           }else{
             if(res.status===200){
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"Usuario",
                   bodymodal:"Tu cuenta fue validada ahora debes ingresar a tu correo y finalizar el proceso.",
                   iconmodal:true
                 });
               });
             }else if(res.status===400){
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"Usuario",
                   bodymodal:"El correo electrónico no ha sido registrado.",
                   iconmodal:false
                 });
               });
             }
           }
         });
       }
    }
   }
   toggle() {
     this.setState({
       modal: !this.state.modal,
       send:true
     });
   }
   imagesIcon(){
     if(this.state.iconmodal){
       return (<i className="material-icons iconcheckregis">check_circle_outline</i>)
     }else{
       return (<i className="material-icons iconmodaleregis">error_outline</i>)
     }
   }
  render() {

    return (
      <div className="app flex-row align-items-center ">

        <Container>

          <Row className="justify-content-center">

            <Col md="6">
              <Card className="anchoRecupe">

                <CardBody >
                <Row>
                <Col className="text-center">
                <img id = "logoForgetPass" src="./logointelligexLogin.png" className="text-center" alt="Logo Intelligex" />
                </Col>
                </Row>
                  <h1 id="titleForgetPass"className="text-left mt-3">Recuperación de cuenta</h1>
                  <p className="text-muted">Para recuperar tu contraseña debes ingresar el correo electrónico de tu cuenta.</p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>@</InputGroupText>
                    </InputGroupAddon>
                    <Input type="text" invalid={this.state.expEmailV} placeholder="Correo electrónico" value={this.state.email} onChange={this.handleChangeEmail.bind(this)}  />
                    <FormFeedback>Correo electrónico invalido</FormFeedback>
                  </InputGroup>
                  <Button id="buttonSend" onClick={this.sendEmail}  block>Enviar</Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className=" text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="mt-4 text-aling text-center">
                <p>{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>

    );
  }
}

export default ForgetPass;
