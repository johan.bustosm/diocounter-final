import React, { Component } from 'react';
import { Button, Card, CardBody, Col, Container, Input, InputGroup, InputGroupAddon, InputGroupText, Row, Modal, ModalHeader, ModalBody, ModalFooter, FormFeedback } from 'reactstrap';
import {Redirect} from 'react-router-dom'
import request from 'superagent';
import logo from '../../../assets/img/brand/logo.svg'
import env from "../../../env.json"
import "./npass.css"

const expPass = /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/
class ForgetPass extends Component {
  constructor(props) {
    super();
    this.state = {
      email:"",
      password:"",
      passwordR:"",
      expPassV:false,
      expPasV:false,
      modal:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      expEmailV:false,
      send:false
    };
    this.toggle = this.toggle.bind(this);
   }

  componentWillMount() {
    var params = this.props.location.search.split("/")
    this.setState({
       email:params[0].split("=")[1],
       token:params[1].split("=")[1]
     })
  }
   handleChangePass(event){this.setState({password:event.target.value})
   if(expPass.test(event.target.value)===false){
     this.setState(
       {
         expPasV :true
       }
     )
   }else{
     this.setState(
       {
         expPasV:false
       }
     )
   }
 }
   handleChangePassRe(event){this.setState({   passwordR:event.target.value}) }
   sendEmail=()=>{
    if(this.state.password==="" || this.state.email===""){
      this.setState((prevState, props) => {
      return ({
          modal:true,
          titlemodal:"Campos vacios",
          bodymodal:"Debe completar todos los campos",
          iconmodal:false
        });
      });
    }else{
      var count = 0
      if(this.state.passwordR===this.state.password){
        this.setState(
          {
            expPassV :false
          }
          )
      }else{
        this.setState(
          {
            expPassV:true
          }
        )
        console.log("password");
        count = count +1
      }

       if(count>=1){
         return true
       }else{
         var joinUser = 'token ' + this.state.token
         request
         .post(env.http+"changepass")
         .set('authorization', String(joinUser))
         .send({email: this.state.email, password:this.state.password })
         .end((err, res) => {
           console.log(res);
           if(res===undefined || res.status===500){
             this.setState((prevState, props) => {
               return ({
                 modal:true,
                 titlemodal:"Error en el servidor",
                 bodymodal:"Intentelo más tarde",
                 iconmodal:false
               });
             });
           }else{
             if(res.status===200){
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"Usuario",
                   bodymodal:"Tu contraseña ya fue actualizada.",
                   iconmodal:true,
                   send:true
                 });
               });
             }else if(res.status===400){
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"Usuario",
                   bodymodal:"El correo electrónico no ha sido registrado.",
                   iconmodal:false
                 });
               });
             }else if(res.status===401){
               this.setState((prevState, props) => {
                 return ({
                   modal:true,
                   titlemodal:"Usuario",
                   bodymodal:"El tiempo para la modificación de su contraseña se ha vencido.",
                   iconmodal:false
                 });
               });
             }
           }
         });
       }
    }
   }
   toggle() {
     this.setState({
       modal: !this.state.modal,

     });
   }
   imagesIcon(){
     if(this.state.iconmodal){
       return (<i className="material-icons iconcheckregis">check_circle_outline</i>)
     }else{
       return (<i className="material-icons iconmodaleregis">error_outline</i>)
     }
   }
  render() {
    if(this.state.send && !this.state.modal){
        return(<Redirect to='/login'/>)
    }
    return (
      <div className="app flex-row align-items-center ">

        <Container>

          <Row className="justify-content-center">

            <Col md="6">
              <Card className=" anchoRecupe">

                <CardBody >
                <Row>
                <Col className="text-center">
                <img id = "user" src={logo} className="text-center" alt="Logo Intelligex" />
                </Col>
                </Row>
                  <h1 className="text-center mt-3">Nueva contraseña</h1>
                  <p className="text-muted">Ingresa tu nueva contraseña</p>
                  <InputGroup className="mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="icon-lock"></i>
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input  type="password" onCopy="return false" invalid={this.state.expPasV}  placeholder="Contraseña" value={this.state.password} onChange={this.handleChangePass.bind(this)}  />
                    <FormFeedback>La contraseña debe tener entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.</FormFeedback>
                  </InputGroup>
                    <InputGroup className="mb-4">
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="icon-lock"></i>
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input type="password"  onpaste="return false"  invalid={this.state.expPassV} placeholder="Repetir contraseña" value={this.state.passwordR} onChange={this.handleChangePassRe.bind(this)} />
                      <FormFeedback>No coinciden las contraseñas</FormFeedback>
                    </InputGroup>
                  <Button onClick={this.sendEmail} color="primary" block>Guardar</Button>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className=" text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="mt-4 text-aling text-center">
                <p>{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>

    );
  }
}

export default ForgetPass;
