import XLSX from 'xlsx';
export const xlsGeneral = (camera,app,date,dataGrafic,dataEmotion,selectFilterEmo,categoryType,categoryTimer) => {
    
    var options = {
        year: "numeric",
        month: "2-digit",
        day: "numeric"
    };
    let dataExport = []
    dataExport.push(["Reporte Analitica de Audiencias"], [])
    dataExport.push(["Camara",camera])
    dataExport.push(["Nombre de Aplicación",app], [])
    dataExport.push(["Fecha de inicio",date.start.toDate().toLocaleDateString("es", options),"Fecha de final",date.end.toDate().toLocaleDateString("es", options)], [])
    dataExport.push(["Clientes"], [])
    dataExport.push(["Categoria",
    categoryType==="total" ? "Todos" : categoryType==="gender" ? "Genero" : "Edades" ,
    categoryTimer==="mes"?"Mes":"Dia"], [])
    if(categoryType==="total"){
        dataExport.push(["Fecha","Clientes"], [])
        dataGrafic.map((obj)=>{
            dataExport.push([obj.name,obj.cantidad])
        })
    }else if(categoryType==="gender"){
        dataExport.push(["Fecha","Hombres","Mujeres"], [])
        dataGrafic.map((obj)=>{
            dataExport.push([obj.name,obj.hombre,obj.mujer])
        })
    }if(categoryType==="age"){
        dataExport.push(["Fecha","0-12","13-22","23-32","33-42","43-52","53+"], [])
        dataGrafic.map((obj)=>{
            dataExport.push([obj.name,obj["0-12"],obj["13-22"],obj["23-32"],obj["33-42"],obj["43-52"],obj["53+"]])
        })
    }

    dataExport.push([])
    dataExport.push(["Emociones"], [])
    dataExport.push(["Categoria",selectFilterEmo===3 ? "Todos" : selectFilterEmo===1 ? "Hombre" : "Mujer" ], [])
    dataExport.push(["Emocion","Porcentaje" ,"","Tiempo de Permanencia", dataEmotion.timer ,"","Edad Promedio",dataEmotion.category], [])
     if(dataEmotion.emotion.length>0){
         dataExport.push(['Alegria',dataEmotion.emotion[0]])
         dataExport.push(['Neutral',dataEmotion.emotion[1]])
         dataExport.push(['Tristeza',dataEmotion.emotion[2]])
         dataExport.push(['Ira',dataEmotion.emotion[3]])
         dataExport.push(['Sorpresa',dataEmotion.emotion[4]])
         dataExport.push(['Miedo',dataEmotion.emotion[5]])
         dataExport.push(['Disgusto',dataEmotion.emotion[6]])
     }
    /*dataExport.push(["#", "Fecha", "Fecha estimada", "Cliente", "Centro de costo", "Estado", "Apr/Can", "Costo"], [])
    data.map((obj, key) => {
      var init = 0
      if (obj._id.length - 5 > 0) {
        init = obj._id.length - 5
      }
      let fechaInit = moment(obj.createdAt).format("lll")
      let fechaExtima = moment(obj.createdAt).add(5, 'days').format("lll")
      let status = "";
      let fechaAcepRec
      switch (obj.status) {
        case "inAproved":
          status = "En aprobación";
          fechaAcepRec = "En espera"
          break;
        case "approved":
          status = "Aprobado";
          fechaAcepRec = moment(obj.apr).format("lll")
          break;
        case "reject":
          status = "Rechazado";
          fechaAcepRec = moment(obj.apr).format("lll")
          break;
      }
      let amount = accounting.formatMoney(obj.amount, '$', 0)
  
      dataExport.push([
        obj._id.substring(init, obj._id.length),
        fechaInit,
        fechaExtima,
        obj.centerCostUser,
        obj.centerCostName,
        status,
        fechaAcepRec,
        amount
      ])
    }) */

    const ws = XLSX.utils.aoa_to_sheet(dataExport);
    const wb = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, "Resumen");
    /* generate XLSX file and send to client */
    XLSX.writeFile(wb, `ReporteAnalitica.xlsx`)
  
  
  }