import React, { Component } from 'react';
import {
  Row,
  Col,
  Button,
  Alert,
  Card,
  CardTitle,
  CardText,  Modal, ModalHeader, ModalBody, ModalFooter,
  ButtonGroup,
ButtonDropdown,
DropdownToggle,
DropdownMenu,DropdownItem
} from 'reactstrap';
import request from 'superagent'
import {Link} from 'react-router-dom';
import store from '../../store';
import env from "../../env.json";
import './camera.css'
class Cameras extends Component {
  constructor() {
    super();
    this.state = {
      camera:[],
      project:store.getState().project,
      checked: false,
      modal:false,
      modalAdver:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      idtemp:""
    }
    this.handleChange = this.handleChange.bind(this);
    this.toggle = this.toggle.bind(this);

  }
  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }
  imagesIcon() {
      if (this.state.iconmodal === "info") {
        return ( < i className = "material-icons infomo" > info < /i>)
        }
        else if (this.state.iconmodal === "warning") {
          return ( < i className = "material-icons icowarni" > warning < /i>)
          }
          else if (this.state.iconmodal === "check_circle_outline") {
            return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
            }else if(this.state.iconmodal==="error_outline"){
              return (<i className="material-icons iconerror">error_outline</i>)
            }
  }
  componentDidMount(){
    this.cameras();
  }
  /*
  Metodos
  */
  /*Ver todas las camaras*/
  cameras=()=>{
    // console.log(this.state.project.id )
    request
     .post(env.http+"alldevices")
     .send({ project_id: this.state.project.id }) // sends a JSON post body
     .end((err, res) => {
       if (err) {

       }else{

         if(res.body.code===200){
           this.setState({
             camera:res.body.message
           })

         }
       }
   });
  }
  deleDevice=(idDevice,state)=>{
    if(state){
      this.setState({
        modal: true,
        titlemodal:"Usuario",
        bodymodal:"Señor usuario, recuerde que primero debe apagar todas sus aplicaciones.",
        iconmodal:"warning"
      })
    }else{

      this.setState({
        modalAdver: true,
        titlemodal:"Usuario",
        bodymodal:"Señor usuario, esta seguro de eliminar la cámara.",
        iconmodal:"warning",
        idtemp:idDevice
      })
    }

  }
  cancelDele=()=>{
    this.setState({
      modalAdver: false,
    })
  }

  acepConfiDele=()=>{
    this.setState({
      modalAdver: false,
    })
    var user = sessionStorage.getItem("token");
    // var user = this.state.token
    var joinUser ='token '+user
    request
     .delete(env.http+"devices")
     .set('authorization',String(joinUser))
     .send({ device_id: this.state.idtemp }) // sends a JSON post body
     .end((err, res) => {
       if(err){
         console.log("err-devices::",err)
       }else{

         if(res.body.code===200){
           this.cameras();
         }
       }
   });

  }

updateDesk=(device,state)=>{
    // this.setState({
    //   modalAdver: false,
    // })
    if(state){
      this.setState({
        modal: true,
        titlemodal:"Usuario",
        bodymodal:"Señor usuario, recuerde que primero debe apagar todas sus aplicaciones.",
        iconmodal:"warning"
      })
    }else{
      var user = sessionStorage.getItem("token");
      var joinUser ='token '+user
      request
       .post(env.http+"updatedesk")
       .set('authorization',String(joinUser))
       .send({ "device":device }) // sends a JSON post body
       .end((err, res) => {
         if(err){
           console.log("err-devices::",err)
         }else{
  
           if(res.status===200){
             this.cameras();
           }
         }
     });
      }


  }
  handleChange(id,checkedcam) {
    request
     .post(env.http+"powerdevices")
     .send({ id_camera: id , power:!checkedcam}) // sends a JSON post body
     .end((err, res) => {
       if(err){
         console.log("err-powerdevices::",err)
       }else{

         if(res.body.code===200){
           this.cameras();
         }
       }
   });
   }
  /*Estado de la camaras*/
  circulo=(state)=>{
    if(state){
      return(<div className="text-center"><div className="circuloG text-center ml-4"></div>Encendida</div>)
    }
     else {
       return(<div className="text-center"><div className="circuloR text-center ml-3"></div>Apagada</div>)
    }
  }
  circuloVali=(state)=>{
    if(!state){
      return(<div className="text-left"><div className="circuloGra text-center ml-4"></div>Sin validar</div>)
    }
  }
  alertValidation(){
    if(this.state.camera.length===0){
      return(
        <Alert className="noteP" color="dark">
          Señor usuario, actualmente no cuenta con ninguna cámara configurada recuerde que primero debe agregar una nueva.
        </Alert>
      )

    }
  }

  toogleSettin=(key)=>{
    // console.log("state",this.state);
    
    // console.log("value",key);
    // console.log("value",typeof(this.state[key]));
    // var obj = {} 
    this.state[key] = !this.state[key]
    // console.log("obj",obj)
    this.setState({
      key : ! this.state[key]
    })
  }

  render(){
    sessionStorage.removeItem("temp")
    const data = this.state.camera.map((obj,index)=>{

      // const newTo = {
      //                pathname: "",
      //                param1: {name:obj.name,id:String(obj._id)}
      //                };
      if(obj.vali===false){
        return(
           <Col sm="3" md="3" key={index}>
               <Card body className="cardStyle" >
                <CardTitle>{obj.name.slice(0, 15)}
                <ButtonGroup className="float-right" style={{"border":"0px"}}>
              <ButtonDropdown id={`card${index}`} isOpen={this.state[`card${index}`] == undefined ? this.state[`card${index}`]= false :  this.state[`card${index}`]} toggle={() => { this.toogleSettin(`card${index}`) }}>
                    <DropdownToggle caret className="p-0 button-dropmenu" color="white" > 
                    <i className="icon-settings animate-trasla "></i>                      
                    </DropdownToggle>
                    <DropdownMenu right>
                    <DropdownItem onClick={()=>this.cameras()}>
                      <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>check_circle_outline</i>
                      Verificar
                      </DropdownItem>
                      <DropdownItem onClick={()=>this.deleDevice(obj._id,obj.state)}>
                      <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>delete_outline</i>
                      Eliminar
                      </DropdownItem>
                    </DropdownMenu>
                  </ButtonDropdown>
                </ButtonGroup>
                </CardTitle>
                <CardText>
                  <Row>
                    <Col className="text-center" >
                  <i className="material-icons icon iconvali">{obj.image}</i>
                    </Col>
                  </Row>
                  <Row>
                  <Col md="8" >
                  {this.circuloVali(obj.vale)}
                  </Col >
                    <Col className="text-left">
                    {/* <i onClick={()=>this.deleDevice(obj._id,obj.state)}className="material-icons icondele text-right">delete_outline</i> */}
                    </Col>
                  </Row>


                </CardText>
              </Card>
            </Col>
        )
      }else{

      return(
         <Col sm="3" md="3" key={index}>
            <Card body className="cardStyle" >
          
              <CardTitle>{obj.name.slice(0, 15)}
              <ButtonGroup className="float-right" style={{"border":"0px"}}>
              <ButtonDropdown id={`card${index}`} isOpen={this.state[`card${index}`] == undefined ? this.state[`card${index}`]= false :  this.state[`card${index}`]} toggle={() => { this.toogleSettin(`card${index}`) }}>
                    <DropdownToggle caret className="p-0 button-dropmenu" color="white" > 
                    <i className="icon-settings animate-trasla "></i>                      
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem onClick={()=>{  this.updateDesk(obj["_id"],obj.state);
                      }}><span>
                        <i class="material-icons" style={{"fontSize":"20px","color":"#000000"}}>
                      autorenew
                      </i>Act. Desktop</span></DropdownItem>
                      <DropdownItem onClick={()=>this.deleDevice(obj._id,obj.state)}>
                      <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>delete_outline</i>
                      Eliminar
                      </DropdownItem>
                      {/* <DropdownItem>Editar</DropdownItem> */}
                    </DropdownMenu>
                  </ButtonDropdown>
                </ButtonGroup>
              </CardTitle>

              <CardText>
                <Row>
                  <Col className="text-center" >
                <Link to="/apps"><i className="material-icons icon iconsinvali">{obj.image}</i></Link>
                  </Col>
                </Row>
                <Row>
                <Col >
                {this.circulo(obj.state)}
                </Col >
                <Col >
                </Col >
                  <Col className="text-right">
                  {/* <i onClick={()=>this.deleDevice(obj._id,obj.state)}className="material-icons icondele text-right">delete_outline</i> */}
                  </Col>
                </Row>

              </CardText>
            </Card>
          </Col>
      )
    }
    })

    return(
      <div>
        <Row>
          <Col >  <span className="display-4 ">Cámaras</span> </Col>
        </Row>
        <Row className="mt-2">
                  <Col>
                  <span className="titleTarCamera">{`Proyecto: ${this.state.project.name[0].toUpperCase()}${this.state.project.name.slice(1,this.state.project.name.length)}`}</span>
                  </Col>
        </Row>
                
        <Row className="mt-4">
          {data}
        </Row>
        <Row>
          <Col className="text-right">
            <Link to="/newcamera"><i className="material-icons icon">add_circle</i></Link>
            {this.alertValidation()}
          </Col>

        </Row>
        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal isOpen={this.state.modalAdver}  className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.acepConfiDele}>Aceptar</Button>
              <Button color="secondary" onClick={this.cancelDele}>Cancelar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    )
  }
}

export default Cameras;
