import React, { Component } from 'react';
import {Form, FormGroup, Label, Input,Row, Col } from 'reactstrap';
import SettingIP from '../ConfigCamera/ConfigIpCam';
import SettingWebCam from '../ConfigCamera/ConfigWebCam';
import store from '../../store';
class newCamera extends Component {
  constructor() {
    super();
    this.state = {
      select:"",
      project:store.getState().project
    };
   }

 habdleSelect(event){

   switch (event.target.value) {
     case "CÁMARA IP":
        this.setState({select:0})
        break;
     case "WEBCAM":
         this.setState({select:1})
         break;
     case "TELEFONO INTELIGENTE":
         this.setState({select:2})
         break;
     default:
            break;
     }
 }
 viewForm(){
   if(this.state.select===0){
    return(<SettingIP project={this.state.project.id} />)
  }else if(this.state.select===1){
     return(<SettingWebCam project={this.state.project.id} />)
   }else if(this.state.select===2){
     return("ESTARÁ PRONTO DISPONIBLE")
   }
 }
 render(){
   return(
     <div>
       <Row>
         <Col></Col>
         <Col>  <h1 className="text-center">Nueva cámara</h1> </Col>
         <Col></Col>
       </Row>
       <Row>
       <Col md="2"></Col>
       <Col md="8">
          <Form>
             <FormGroup>
               <Label for="exampleSelect">Seleccione una cámara </Label>
               <Input type="select"   onChange={this.habdleSelect.bind(this)} name="select" id="exampleSelect">
                 <option></option>
                 <option>CÁMARA IP</option>
                 <option>WEBCAM</option>
               </Input>
             </FormGroup>
         </Form>
         </Col>
        <Col md="2"></Col>
      </Row>
      <Row>
        <Col md="2"></Col>
        <Col md="8">
          {this.viewForm()}
        </Col>
        <Col md="2"></Col>
      </Row>
    </div>
   )

 }}
 export default newCamera;
