import React, { Component } from 'react';
import {Row, Col, Button } from 'reactstrap';
import store from '../../../store';
import Canvas from '../components/canvas';
import CanvasBarra from '../components/canvas/canvasbarrera.js';
import Config from '../components/config';
import ConfigAnalityAudien from '../components/config/AnalityAudien';
import LoaderStep from '../components/pasos';
import Precing from '../components/pago';
import "../css/newapp.css";
class newApp extends Component {
  constructor(props) {
    super();
    this.state = {
      idDevice:sessionStorage.getItem('deviceApp'),
      project:store.getState().project,
      idtemp:undefined,
      type:sessionStorage.getItem('idApp'),
      image:sessionStorage.getItem('imageApp'),
      image_src:"photo-camera.png",
      step:store.getState().step
    };

   }
   componentDidMount() {
     store.subscribe(()=> this.nextStep(store.getState()) )
   }
   componentWillUnmount() {
     store.dispatch({
       type:"CONFIG_APP",
       step:1
     })
   }
   nextStep= (db) =>{
     this.setState((state, props) => ({
       step: db.step,
       idtemp:db.temp,
       // image_src:db.img
     }));
   }

   viewButton(){
     if (this.state.step<3) {
       return (< Button className="mb-4" color="success" onClick={this.nextStep} size="lg" block>Siguiente</Button>)
     }
   }
   viewForm(){
     if(this.state.step===1){
      if(this.state.type==="ANALYTIC AUDIENCES"){
        return(<ConfigAnalityAudien idDevice={this.state.idDevice} idtemp={this.state.idtemp} project={this.state.project} type={this.state.type} image={this.state.image}/>)

      }else{
        return(<Config idDevice={this.state.idDevice} idtemp={this.state.idtemp} project={this.state.project} type={this.state.type} image={this.state.image}/>)
      }
     }else if (this.state.step===2) {
       if(this.state.type==="POLYGON COUNTER" || this.state.type==="ANALYTIC AUDIENCES"){
         return(<Canvas idtemp={this.state.idtemp}  url_image={this.state.image_src} type={this.state.type} />)
       }else{
         return(<CanvasBarra idtemp={this.state.idtemp}  url_image={this.state.image_src} type={this.state.type} />)
       }
     }else if (this.state.step===3) {
       return (<Precing/>)
     }
   }

 render(){
console.log(this.state.step);
   return(
     <div>
     <Row>
     <Col>

      <LoaderStep step={this.state.step}/>
      </Col>
      </Row>
     <Row className="sizeRow">
     <Col md="2"></Col>
     <Col md="8">

     {this.viewForm()}
     </Col>
     <Col md="2"></Col>
     </Row>
     <Row className="mt-3">
     <Col md="5" ></Col>
     <Col md="2">
      {
        // this.viewButton()
      }
     </Col>
     <Col md="5" ></Col>
     </Row>
    </div>
   )

 }}
 export default newApp;
