export default {
  item : [{

      name: `Conteo en área`,
      icono: "filter_center_focus",
      id:"POLYGON COUNTER"
    },
    {
    name: "Conteo en ingresos",
    icono: "flip",
    id:"INCOME COUNTER"
    },
    {
      name: "Analítica de audiencia",
      icono: "portrait",
      id:"ANALYTIC AUDIENCES"
    }
  ]
}
