import React, { Component, Fragment } from 'react';
import {
    Row,
    Col,
    Card,
    CardTitle,
    CardText, Label
} from 'reactstrap';
import { Redirect } from 'react-router-dom';
import app from './apps.js'
import "../css/industrial.css"
export default class Industria extends Component {
    constructor(props) {
        super();
        this.state = {
            apps: app.item,
            redirec: false,
            device: props.location.state.id 
        }
    }

    redirapp=(idapp)=>{
        sessionStorage.setItem('idApp',idapp)
        sessionStorage.setItem('deviceApp',this.state.device)
        this.setState({
          redirec:true
        })
      }

    render() {
        if(this.state.redirec){
            return(<Redirect to={{
                pathname: "/typeapp",
                state: {
                    "id": String(this.state.device),
        
                }
        }}/>)
          }
        return (

            <Fragment>
                <div className="row m-3">
                    <div className="col text-center">
                        <h3 className="display-3">Escoje tu industria</h3>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4 col-sm-12">
                        <Card className="cardindustrial">
                            <Row>
                                <Col className="text-center" >
                                    <i className="material-icons iconoapp" onClick={() => this.redirapp()}>store_mall_directory</i>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="text-center">
                                    <h5>
                                        Retail
                                    </h5> 
                                </Col>
                            </Row>
                        </Card>
                    </div>
                    <div className="col-md-4 col-sm-12">
                        <Card className="cardindustrial">
                            <Row>
                                <Col className="text-center" >
                                    <i className="material-icons iconoapp" onClick={() => this.redirapp()}>fingerprint</i>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="text-center">
                                    <h5>
                                        Seguridad
                                    </h5> 
                                </Col>
                            </Row>
                        </Card>
                    </div>
                    <div className="col-md-4 col-sm-12">
                        <Card className="cardindustrial">
                            <Row>
                                <Col className="text-center" >
                                    <i className="material-icons iconoapp" onClick={() => this.redirapp()}>timeline</i>
                                </Col>
                            </Row>
                            <Row>
                                <Col className="text-center">
                                    <h5>
                                        Marketing
                                    </h5> 
                                </Col>
                            </Row>
                        </Card>
                    </div>
                </div>

            </Fragment>
        )

    }

}