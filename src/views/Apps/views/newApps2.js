import React, { Component } from 'react';
import {Form, FormGroup, Label, Input,Row, Col, Button,CustomInput } from 'reactstrap';
import ReactLoading from 'react-loading';
import request from 'superagent'
import store from '../../../store';
import env from "../../../env.json";
import Canvas from '../components/canvas';
import "../css/newapp.css";
class newApp extends Component {
  constructor(props) {
    super();
    this.state = {
      idDevice:sessionStorage.getItem('deviceApp'),
      idtemp:sessionStorage.getItem("temp"),
      project:store.getState().project,
      flagTakePhoto:false,
      socket:props.http,
      image_src:"photo-camera.png",
      cleanImage:true,
      sendMessage:true,
      visible:false,
      select:sessionStorage.getItem('idApp'),
      model:"",
      image:sessionStorage.getItem('imageApp'),
      alert:false,
      condicional:"none",
      numconficional:"none",
      loader:false,
      email:""
    };

   }
   componentDidMount() {
     this.timerID = setInterval(() => this.receivePhoto(), 500);
   }

   componentWillUnmount() {
     clearInterval(this.timerID);
   }

  receivePhoto() {
    if (this.state.flagTakePhoto) {
      request
        .post(env.http + "deliverPhoto")
        .send({
          temp: this.state.idtemp
        }) // sends a JSON post body
        .end((err, res) => {
          console.log(res);
          if (res.body.code === 200 && res.body.photo !==" " ) {
            this.setState({
              flagTakePhoto: false,
              image_src: "data:image/jpeg;base64," + String(res.body.photo),
              loader:false
            })

          }
        });
    }
  }

   takePicture = () => {
     if(this.state.select==="" || this.state.model===""){
       alert("Debe seleccionar una App y un modelo")
       return true
     }else{

     request
       .post(env.http + "apps")
       .send({
         devices_id: this.state.idDevice,
         type: this.state.select,
         model: this.state.model,
         image: this.state.image,
         temp: this.state.idtemp,
         idpro:this.state.project.id,
         condi :this.state.condicional,
         numcondi:this.state.numconficional,
         email:this.state.email
       }) // sends a JSON post body
       .end((err, res) => {
         console.log(res)
         if (err){
           console.log(err)
         } else{
           if (res.body.code === 200) {

             this.setState({
               flagTakePhoto: true,
               idtemp: res.body.message.id_temp,
               loader:true
             })


              sessionStorage.setItem("temp",res.body.message.id_temp)
           }
           if(res.body.code === 202){
             this.setState({
               flagTakePhoto: true,
             })

           }

         }

       })
      }
   }

   loader(){
     if(this.state.loader){
       return (
         <div>
         <Row>
         <Col></Col>
         <Col>
         <ReactLoading type="bars" width={'100%'} />
         </Col>
         <Col></Col>
         </Row>
         </div>
       )

     }else{
       return (
         <Canvas url_image={this.state.image_src} temp={this.state.idtemp} clean={this.state.cleanImage} send={this.state.sendMessage} image={this.state.image}/>
       )
     }
   }

   clean = () => {
     this.setState({
       cleanImage: !this.state.cleanImage
     })
   }

   send = () => {
     console.log("Estoy cambiando de esto en send")
     this.setState({
       sendMessage: !this.state.sendMessage
     })
   }

 habdleSelect(event) {
   switch (event.target.value) {
     case "Mayor":
       this.setState({
         condicional:">"
       })
       break;
     case "Igual":
       this.setState({
          condicional:"="
       })
       break;
     default:
       break;
   }
 }

 habdleRadioButton(changeEvent) {
   this.setState({
     model: changeEvent.target.value
   })
 }
 handleChecked=(event)=>{
   switch (event.target.id) {
     case "1":
        this.setState({alert:!this.state.alert});
         break;
     default:
        break;
 }
}
handleChangeNumcon(event){this.setState({   numconficional:event.target.value})}
handleChangeEmail(event){this.setState({   email:event.target.value})}

alertConfig(){
  if(this.state.alert){
    return(
      <div>
      <FormGroup>
      <Label for="exampleSelect">Me genere una alerta cuando el contador sea: </Label>
      <Input type="select"   onClick={this.habdleSelect.bind(this)} name="select" id="exampleSelect">
      <option></option>
      <option>Mayor</option>
      <option>Igual</option>
      </Input>
      </FormGroup>
      <FormGroup>
         <Label for="exampleNumber">a</Label>
         <Input type="number" name="number" id="exampleNumber" placeholder="0" onChange={this.handleChangeNumcon.bind(this)} />
       </FormGroup>
       <FormGroup>
         <Input type="text" placeholder="Correo electrónico" value={this.state.email} onChange={this.handleChangeEmail.bind(this)}  />
       </FormGroup>
      </div>
    )
  }
}

 render(){

   return(
     <div>
       <Row>
         <Col></Col>
         <Col>  <h1 className="text-center">Nueva App</h1> </Col>
         <Col></Col>
       </Row>
       <Row >
       <Col className="mt-5">
          <Form>
               <FormGroup tag="fieldset">
                      <Label for="exampleSelect">Seleccione un modelo de IA</Label>
                        <FormGroup check>
                          <Label check>
                            <Input type="radio" name="radio1" value="person" onChange={this.habdleRadioButton.bind(this)} />{' '}
                            Persona
                          </Label>
                        </FormGroup>

                        <FormGroup check>
                          <Label check>
                            <Input type="radio" name="radio1" value="car" onChange={this.habdleRadioButton.bind(this)}/>{' '}
                            Carro
                          </Label>
                        </FormGroup>

                        <FormGroup check>
                          <Label check>
                            <Input type="radio" name="radio1" value="bicycle" onChange={this.habdleRadioButton.bind(this)}/>{' '}
                            Bicicleta
                          </Label>
                        </FormGroup>

                        <FormGroup check>
                          <Label check>
                            <Input type="radio" name="radio1" value="motorcycle" onChange={this.habdleRadioButton.bind(this)}/>{' '}
                            Motocicleta
                          </Label>
                        </FormGroup>

                        <FormGroup check>
                          <Label check>
                            <Input type="radio" name="radio1"  value="bottle" onChange={this.habdleRadioButton.bind(this)}/>{' '}
                            Botella
                          </Label>
                        </FormGroup>


                  </FormGroup>

                  <FormGroup>
                    <Label for="exampleCheckbox">Sistema de alerta</Label>
                    <div>
                    <CustomInput type="checkbox" id="1" onChange={this.handleChecked.bind(this)} label="Activar" />
                    </div>
                  </FormGroup>
                  {this.alertConfig()}

         </Form>

           </Col>
         {/* canvas*/}
          <Col className="mr-5">
          <Row className="mb-4 text-aling text-center">

                  <Col>  {this.loader()}</Col>

          </Row>
         <Row >
                 <Col md="4">
                   < Button className="ml-3 mr-3" color="primary" size="lg" onClick={this.takePicture} size="lg" block><i className="material-icons ">camera_enhance</i>Capturar</Button>
                 </Col>
                 <Col md="4">
                   < Button className="ml-3 mr-3" color="primary" size="lg" onClick={this.clean} size="lg" block><i className="material-icons ">delete_forever</i>Limpiar</Button>
                 </Col>
                   <Col md="4">
                   < Button className="ml-3" color="primary" size="lg" onClick={this.send} size="lg" block><i className="material-icons">send</i>Enviar</Button>
                 </Col>
        </Row>
        </Col>

        </Row>
    </div>
   )

 }}
 export default newApp;
