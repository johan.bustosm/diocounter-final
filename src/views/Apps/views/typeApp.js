import React, { Component } from 'react';
import {
  Row,
  Col,
  Card,
  CardTitle,
  CardText, Label
} from 'reactstrap';
import {Redirect} from 'react-router-dom';
import app from './apps.js'
import "../css/typeapp.css"
import "../css/industrial.css"
class typeApp extends Component {
  constructor(props) {
    super();
    this.state = {
      apps:app.item,
      redirec:false,
      device:props.location.state.id
    }
  }

  /*
  Metodos
  */
  redirapp=(idapp,icono)=>{
    // console.log(idapp,icono,this.state.device)
    sessionStorage.setItem('idApp',idapp)
    sessionStorage.setItem('imageApp',icono)
    sessionStorage.setItem('deviceApp',this.state.device)
    this.setState({
      redirec:true
    })
  }

  render(){
    if(this.state.redirec){
      return(<Redirect to='/newapp'/>)
    }
    const data = this.state.apps.map((obj,index)=>{

      return(
         <Col sm="12" md="4">
            <Card  className="cardindustrial">
              
              
                <Row>
                  <Col className="text-center" >
                  <i className="material-icons iconoapp" onClick={()=>this.redirapp(obj.id,obj.icono)}>{obj.icono}</i>
                  </Col>
                </Row>
                <Row>
                                <Col className="text-center">
                                    <h5>
                                    {obj.name}
                                    </h5> 
                                </Col>
                </Row>
              
            </Card>
          </Col>
      )
    })

    return(
      <div>
             <div className="row m-3">
                    <div className="col text-center">
                        <h3 className="display-3">Apps</h3>
                    </div>
                </div>
        <Row>
          <Col>  <Label for="exampleNumber" className="mr-sm-2 ml-sm-2 labelForm" > Seleccione una aplicación </Label> </Col>
        </Row>
        <Row>
          {data}
        </Row>
      </div>
    )
  }
}

export default typeApp;
