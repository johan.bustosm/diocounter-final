/*
Dependencias
*/
import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Row, Col, Card,CardHeader, Button, CardTitle, CardText, CardDeck, CardBody,Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import request from 'superagent';
import store from '../../../../store.js'
import env from '../../../../env.json'
/*
Estilos
*/
import './pago.css'


class Precios extends Component {
  constructor(props) {
    super();
    this.state = {
      flgareturn:false,
      tempid:store.getState().temp,
      modal:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      version:""
    }
    this.toggle = this.toggle.bind(this);
   }
   toggle() {
     this.setState({
       modal: !this.state.modal
     });
   }
   imagesIcon() {
       if (this.state.iconmodal === "info") {
         return ( < i className = "material-icons infomo" > info < /i>)
         }
         else if (this.state.iconmodal === "warning") {
           return ( < i className = "material-icons icowarni" > warning < /i>)
           }
           else if (this.state.iconmodal === "check_circle_outline") {
             return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
             }else if(this.state.iconmodal==="error_outline"){
               return (<i className="material-icons iconerror">error_outline</i>)
             }
   }
  endProcess=(plan)=>{
    request
      .post(env.http + "saveapp")
      .send({
        temp: this.state.tempid,
        pay: plan
      })
      .end((err, res) => {
        if (err) {
          alert("Hubo un error en la ")
        } else {
          if (res.body.code === 200) {
            if(plan==="free"){
              this.setState({
                flgareturn:true,
                  modal: true,
                titlemodal: "Usuario",
                bodymodal: "Su aplicación fue registrada, no olvide activarla ahora.",
                iconmodal: "check_circle_outline",
                version:"free"
              })
            }else if(plan==="pay"){
              this.setState({
                flgareturn:true,
                  modal: true,
                titlemodal: "Usuario",
                bodymodal: "Su aplicación fue registrada y entra en proceso de validación de pago, una vez realizado el pago tardara entre 5 y 10 mins la activación.",
                iconmodal: "check_circle_outline",
                version:"pay"
              })
            }
          } else {
            this.setState({
              modal: true,
              titlemodal:"Usuario",
              bodymodal:"Hubo un error en el sistema, intente más tarde. ",
              iconmodal:"error_outline"
            })
          }
        }
      })
  }
  pase =()=>{
    if(this.state.version==="free"){
      return(<Button color="primary" onClick={this.toggle}>Aceptar</Button>)
    }else{
      return(
        <Button color="primary" onClick={this.toggle}>Aceptar</Button>
        /* <a href="https://biz.payulatam.com/B0bab9bDED551E4"><Button color="primary">Aceptar</Button></a> */
      )
    }
  }

  render(){

    if(this.state.flgareturn && !this.state.modal){
      return(<Redirect to='/apps'/>)
    }
    return(
      <div>
        <CardDeck className="mt-4 mb-2">
        <Card >
          <CardHeader  className="headerPago" >Intelligex <br/>  Free</CardHeader>
          <CardBody>
            <CardTitle className="text-center titlePrecio"></CardTitle>
            <CardText className="feactures">
            <p>Prueba durante 4 dias</p>
            <hr/>
            <p>Contara con caracteristicas de la versión <b>Intelligex Profesional</b></p>
            <hr/>
            <p>Tu aplicación se borrar junto con su información</p>
            <hr/>

            </CardText>
            <Row>
            <Col></Col>
            <Col>
            <Button onClick={()=>this.endProcess("free")} className="mt-2" color="primary">Suscribirse</Button>
            </Col>
            <Col></Col>
            </Row>
          </CardBody>
        </Card>
        <Card>
          <CardHeader className="headerPagoP">Intelligex <br/>  Profesional</CardHeader>
          <CardBody>
            <CardTitle className="text-center titlePrecio"></CardTitle>
            <CardText className="feactures">
            <p>Paga una vez por mes</p>
            <hr/>
            <p>Obtén informe historico sobre tu información</p>
            <hr/>
            <p>Obtén las detecciones en tiempo real</p>
            <hr/>
            <p>Generación de alertas en tu correo</p>
            <hr/>
            <p>Soporte de tu aplicación</p>
            <hr/>
            </CardText>
            <Row>
            <Col></Col>
            <Col>
            <Button onClick={()=>this.endProcess("pay")} className="mt-2" color="primary">Suscribirse</Button>
            {
              //<img src="http://www.payulatam.com/img-secure-2015/boton_pagar_mediano.png" onClick={()=>this.endProcess("pay")}/>
            }
            </Col>
            <Col></Col>
            </Row>
          </CardBody>
        </Card>
        {


          // <Card>
          //   <CardHeader  className="headerPago" >Watcher Empresarial</CardHeader>
          //   <CardBody>
          //     <CardTitle className="text-center titlePrecio">$ 110.75</CardTitle>
          //     <CardText className="feactures">
          //     <p>Paga una vez por año</p>
          //     <hr/>
          //     <p>Ahorra hasta un 30%</p>
          //     <hr/>
          //     <p>Obten las detecciones en tiempo real</p>
          //     <hr/>
          //     <p>Generación de alertas en tu correo</p>
          //     <hr/>
          //     <p>Generación de reportes de lo sucedido</p>
          //     </CardText>
          //     <Row>
          //     <Col></Col>
          //     <Col>
          //     <Button onClick={this.endProcess} className="mt-2" color="primary">Suscribirse</Button>
          //     </Col>
          //     <Col></Col>
          //     </Row>
          //   </CardBody>
          // </Card>

        }
        </CardDeck>
   <div>
     <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
       <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
       <ModalBody>
         <Row>
         <Col className="md-2 p-0 text-aling text-center">
         {this.imagesIcon()}
         </Col>
         <Col className="md-10 p-0 mt-4 text-aling text-left">
           <p className="bodymessa">{this.state.bodymodal}</p>
         </Col>
         </Row>
       </ModalBody>
       <ModalFooter>
       <Button color="primary" onClick={this.toggle}>Aceptar</Button>
          {
           /* / this.pase() */
          }

       </ModalFooter>
     </Modal>
   </div>
   </div>
    )
  }

}
export default Precios;
