
/*
Dependencias
*/
import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import {Row, Col, Button, Label, Alert,Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import ReactLoading from 'react-loading';
import request from 'superagent';
import store from '../../../../store.js'
/*
Estilos
*/
import './canvas.css'

/*
Variables de env
*/
import env from "../../../../env.json"
var miLienzo
var contexto
var coordenadas=[]
var coordenadasLine=[]
var canvasLimites;
var img
var flag= true ;
var num=0;

class Canvas extends Component{
    constructor(props) {
        super(props);
        this.state = {
          state_coordenadas:[],
          state_rect:true,
          state_line:false,
          flagTakePhoto:false,
          returnapp:false,
          loader:false,
          image_src:"photo-camera.png",
          tempid:store.getState().temp,
          modal:false,
          timeout:false,
          titlemodal:"",
          bodymodal:"",
          iconmodal:""
        };
        this.toggle = this.toggle.bind(this);
       }
       toggle() {
         this.setState({
           modal: !this.state.modal
         });
       }
       imagesIcon() {
           if (this.state.iconmodal === "info") {
             return ( < i className = "material-icons infomo" > info < /i>)
             }
             else if (this.state.iconmodal === "warning") {
               return ( < i className = "material-icons icowarni" > warning < /i>)
               }
               else if (this.state.iconmodal === "check_circle_outline") {
                 return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
                 }
                 else if (this.state.iconmodal === "error_outline") {
                   return ( < i className = "material-icons iconerror" > error_outline < /i>)
                   }
   }
    componentDidMount() {
      this.prepareCanvas();
      this.timerID = setInterval(() => this.receivePhoto(), 500);
    }

      componentWillUnmount() {
        clearInterval(this.timerID);
      }

    receivePhoto() {
      if (this.state.flagTakePhoto) {
        console.log("Haciendo solicitd de fotos");
        request
          .post(env.http + "deliverPhoto")
          .send({
            temp: this.state.tempid
          }) // sends a JSON post body
          .end((err, res) => {
            // console.log(res);
            if (err) {

            } else {
              if (res.status === 200 && res.body.photo !== " ") {
                this.setState({
                  flagTakePhoto: false,
                  loader: false,
                  image_src: "data:image/jpeg;base64," + String(res.body.photo)
                })
                this.prepareCanvas();
              }
            }
          });
      }
    }
    loader(){
      if(this.state.loader){
        return (
          <div className="text-center mt-4 sizeRowLoader" >
          <ReactLoading type="bars" width={'90%'}  />
          </div>
        )

      }else{
        return (
          <div className="text-center ">
          <canvas ref="lienzo" onClick={this.cambiarEstado} onMouseMove={this.pintando} onContextMenu={this.handleStateDraw} />
          <img ref="imagen" src={this.state.image_src} className="hidden" alt="imagen de la camra" />
          </div>
        )
      }
    }
    alertInfo(){
      if(this.state.image_src!=="photo-camera.png" ){
        return(
          <Alert className="mt-1" color="primary">
          Arrastrar el cursor sobre la fotografia, para seleccionar el área de detección.
          </Alert>
        )
      }
    }
    takePicture=()=>{
      this.setState({image_src:""})
      request
        .post(env.http + "apps")
        .send({
          temp: this.props.idtemp
        }) // sends a JSON post body
        .end((err, res) => {
          //console.log(res)
          if (err){
            //console.log(err)
          } else{
            if (res.body.code === 202) {

              this.setState({
                flagTakePhoto:true,
                loader:true
              })
                // sessionStorage.setItem("temp",res.body.message.id_temp)
            }else if(res.status===203){
              this.setState({
                modal: true,
                titlemodal:"Usuario",
                bodymodal:"El tiempo de configuración se acabo debe ingresar todo nueva mente. ",
                iconmodal:"warning",
                timeout:true
              })
            }
          }
        })
    }
    borrar = () => {
      this.setState({
        state_rect: true,
        state_coordenadas: []
      })
      contexto.clearRect(0, 0, miLienzo.width, miLienzo.height);
      img.src = this.state.image_src;
      img.onload();
      contexto.drawImage(img, 0, 0);
    }
    prepareCanvas() {
      miLienzo = this.refs.lienzo;
      contexto = miLienzo.getContext("2d"); // obtenemos el contexto ( dibujar en 2d)
      img = this.refs.imagen;
      img.onload = ()=> {
          miLienzo.width = img.width;
          miLienzo.height = img.height;
          contexto.drawImage(img, 0, 0);
      };
      canvasLimites = miLienzo.getBoundingClientRect(); // obtenemos los limites del canvas
      miLienzo.addEventListener('mousedown', this.cambiarEstado);
      this.refs.lienzo.getBoundingClientRect()
    }
    cambiarEstado=(mouseEvent) =>{
      if (flag && mouseEvent.buttons===1 && this.state.image_src !== "photo-camera.png"){
          flag = false;
          canvasLimites = miLienzo.getBoundingClientRect();
          if (this.state.state_rect){
            coordenadas = []
            console.log("Guardando coordenadas rect")
            coordenadas.push({
              x: mouseEvent.clientX - canvasLimites.left,
              y: mouseEvent.clientY - canvasLimites.top
            });
          }else{
            coordenadasLine = []
            coordenadasLine.push({
              x: mouseEvent.clientX - canvasLimites.left,
              y: mouseEvent.clientY - canvasLimites.top
            });
          }
        }
        else{
          num+=1
          flag=true;

            if (this.state.state_rect){
              coordenadas.push({
                x: mouseEvent.clientX - canvasLimites.left,
                y: mouseEvent.clientY - canvasLimites.top
              });
              console.log(coordenadas)

            }else{
              coordenadasLine.push({
                x: mouseEvent.clientX - canvasLimites.left,
                y: mouseEvent.clientY - canvasLimites.top
              });
            }
          }
          this.setState({ state_coordenadas: coordenadas});
          if(num===1){
            num=0
          }
    }
    pintando=(mouseEvent)=>{
      if(flag===false){
        canvasLimites = miLienzo.getBoundingClientRect();
        contexto.clearRect(0, 0, miLienzo.width, miLienzo.height);
        contexto.drawImage(img, 0, 0);
        contexto.beginPath(); // comenzamos a dibujar
        if(this.state.state_rect){
          contexto.strokeRect(coordenadas[0].x, coordenadas[0].y, (mouseEvent.clientX - canvasLimites.left) - coordenadas[0].x, (mouseEvent.clientY - canvasLimites.top)-coordenadas[0].y);
        }else{
          console.log(coordenadas)
          contexto.strokeRect(coordenadas[0].x, coordenadas[0].y, coordenadas[1].x  - coordenadas[0].x, coordenadas[1].y - coordenadas[0].y);
          contexto.moveTo(coordenadasLine[0].x, coordenadasLine[0].y); // ubicamos el cursor en la posicion (10,10)
          contexto.lineTo((mouseEvent.clientX - canvasLimites.left), (mouseEvent.clientY - canvasLimites.top));
        }
        contexto.strokeStyle = "rgb(51,240,99)"; // color de la linea
        contexto.lineWidth=8;
        contexto.stroke(); // dibujamos la linea
      }
    }

  handleStateDraw = () => {
    this.setState({
      state_rect: !this.state.state_rect
    })
  }

  nextStep= () =>{
    var  tamañoCoordenadas = Object.values(this.state.state_coordenadas)
    if (tamañoCoordenadas.length===2 ) {
      var settings = {
        "barrera": {
          "x1": parseInt(this.state.state_coordenadas[0].x,10).toString(),
          "y1": parseInt(this.state.state_coordenadas[0].y,10).toString(),
          "x2": parseInt(this.state.state_coordenadas[1].x,10).toString(),
          "y2": parseInt(this.state.state_coordenadas[1].y,10).toString()
        }
      }
      request
        .post(env.http + "upconfig")
        .send({
          temp: this.state.tempid,
          config: [settings]
        })
        .end((err, res) => {
          if (err) {
            this.setState({
              modal: true,
              titlemodal:"Usuario",
              bodymodal:"Hubo un error en el sistema, intente más tarde. ",
              iconmodal:"error_outline"
            })
          } else {
            if (res.body.code === 200) {

              store.dispatch({
                type:"CONFIG_APP",
                step:3,
                temp:this.state.tempid
              })
            }else if(res.status===203){
              this.setState({
                modal: true,
                titlemodal:"Usuario",
                bodymodal:"El tiempo de configuración se acabo debe ingresar todo nueva mente. ",
                iconmodal:"warning",
                timeout:true
              })
            } else {
              this.setState({
                modal: true,
                titlemodal:"Usuario",
                bodymodal:"Hubo un error en el sistema, intente más tarde. ",
                iconmodal:"error_outline"
              })
            }
          }
        })
    } else {
      flag=true;
      coordenadas = []
      this.borrar();
      this.setState({
        modal: true,
        titlemodal:"Usuario",
        bodymodal:"Debe terminar el proceso de selección del área de detección.",
        iconmodal:"warning"
      })
    }
  }
    render(){
      if(this.state.timeout && !this.state.modal){
        return(<Redirect to='/apps'/>)
      }else{
        return (

        <div className="sizeRowLoader">
        <Label for = "name" className="labelForm" > Configure el área de detección < /Label>
        <Row >
        <Col>
        </Col>
        <Col>
          {this.loader()}

        </Col>
        <Col>
        </Col>
        </Row>
        <Row>
        <Col md="2">
        </Col>
        <Col md="8">
        {
          this.alertInfo()
        }
        </Col>
        <Col md="2">
        </Col>
        </Row>
            <Row>
                    <Col md="6">
                      < Button className="ml-3 mr-3" color="primary"  onClick={this.takePicture} size="lg" block><i className="material-icons ">camera_enhance</i>Capturar</Button>
                    </Col>
                    <Col md="6">
                      < Button className="ml-3 mr-3" color="primary"  onClick={this.borrar} size="lg" block><i className="material-icons ">delete_forever</i>Limpiar</Button>
                    </Col>
           </Row>
            <Row className="mt-3">
            <Col md="" ></Col>
            <Col md="">
             {
               // this.viewButton()
               < Button className="mb-4 mt-2" color="success"  onClick={this.nextStep} size="lg" block>Siguiente</Button>
             }
            </Col>
            <Col md="" ></Col>
            </Row>
            <div>
              <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
                <ModalBody>
                  <Row>
                  <Col className="md-2 p-0 text-aling text-center">
                  {this.imagesIcon()}
                  </Col>
                  <Col className="md-10 p-0 mt-4 text-aling text-left">
                    <p className="bodymessa">{this.state.bodymodal}</p>
                  </Col>
                  </Row>
                </ModalBody>
                <ModalFooter>
                  <Button color="primary" onClick={this.toggle}>Aceptar</Button>
                </ModalFooter>
              </Modal>
            </div>
        </div>
        );
      }
    }
};

export default Canvas;
