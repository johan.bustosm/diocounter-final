import React, { Component } from 'react';
import "./pasos.css"
class PasosApp extends Component{
    // constructor(props) {
    //   // super(props);
    // }
    loaderStep(){
      if(this.props.step===1){
            return(
              <div  className="text-center" ><div className="circuloB"></div><div className="circuloGray"></div><div className="circuloGray"></div></div>
            )
      }else if(this.props.step===2){
            return(
              <div className="text-center"><div className="circuloGray"></div><div className="circuloB"></div><div className="circuloGray"></div></div>
            )
      }else if (this.props.step===3) {
            return(
              <div  className="text-center"><div className="circuloGray"></div><div className="circuloGray"></div><div className="circuloB"></div></div>
            )
      }
    }
    render(){
        return (
          <div>
          {this.loaderStep()}
          <hr/>
          <h2 className="text-center titlepasos">Paso {this.props.step}</h2>
          </div>
        )
    }
  }
  export default PasosApp;
