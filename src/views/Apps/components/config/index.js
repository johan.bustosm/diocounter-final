/*
Dependencias
*/
import React, { Component } from 'react';
import {Form, FormGroup, Label, Input,Row, Col, Button,CustomInput,FormFeedback,Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import request from 'superagent';
import store from '../../../../store.js';
import env from '../../../../env.json'
import "./confiapp.css"
const expName = /^[a-zA-Z ]*$/;
const expEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

class ConfigApp extends Component{
    constructor(props) {
        super(props);
        this.state = {
          name:"",
          email:"",
          category:" ",
          nameCategory:"",
          alert:false,
          condicional:"none",
          numconficional:0,
          expNameV:false,
          expEmailV:false,
          modal:false,
          titlemodal:"",
          bodymodal:"",
          iconmodal:""

        };
        this.toggle = this.toggle.bind(this);
       }
       toggle() {
         this.setState({
           modal: !this.state.modal
         });
       }
       imagesIcon() {
           if (this.state.iconmodal === "info") {
             return ( < i className = "material-icons infomo" > info < /i>)
             }
             else if (this.state.iconmodal === "warning") {
               return ( < i className = "material-icons icowarni" > warning < /i>)
               }
               else if (this.state.iconmodal === "check_circle_outline") {
                 return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
                 }else if(this.state.iconmodal==="error_outline"){
                   return (<i className="material-icons iconerror">error_outline</i>)
                 }
       }
       habdleSelect(event) {
         switch (event.target.value) {
           case "Mayor":
             this.setState({
               condicional:">"
             })
             break;
           case "Igual":
             this.setState({
                condicional:"="
             })
             break;
             case "Persona":
               this.setState({
                 category:"person",
                 nameCategory:"Personas"
               })
               break;
             case "Carro":
               this.setState({
                  category:"car",
                  nameCategory:"Carros"
               })
               break;
               case "Bicicleta":
                 this.setState({
                   category:"bicycle",
                   nameCategory:"Bicicletas"
                 })
                 break;
               case "Motocicleta":
                 this.setState({
                    category:"motorcycle",
                    nameCategory:"Motocicletas"

                 })
                 break;
               case "Botella":
                 this.setState({
                    category:"bottle",
                    nameCategory:"Botellas"
                 })
                 break;
               case " ":
                   this.setState({
                      category:" ",
                      nameCategory:" "
                   })
                   break;
           default:
             break;
         }
       }
       handleChecked=(event)=>{
         switch (event.target.id) {
           case "1":
              this.setState({alert:!this.state.alert});
               break;
           default:
              break;
       }
      }
      handleChangeNumcon(event){
        if(event.target.value>-1){
          this.setState({numconficional:event.target.value})
        }

      }
      handleChangeEmail(event){
        this.setState({   email:event.target.value})
        if(expEmail.test(event.target.value)===false){
          this.setState(
            {
              expEmailV :true
            }
          )
        }else{
          this.setState(
            {
              expEmailV:false
            }
          )
        }
     }
     handleChangeName(event){
     this.setState({name:event.target.value})
     if(expName.test(event.target.value)===false){
       this.setState(
         {
           expNameV :true
         }
       )
     }else{
       this.setState(
         {
           expNameV:false
         }
       )
     }
   }
   hideAlert=()=>{
     if(this.props.type=="POLYGON COUNTER"){
       return(
        <div> 
        <FormGroup>
        <Label for="exampleCheckbox" className="labelForm" >Sistema de alerta</Label>
        <CustomInput type="checkbox" id="1" onChange={this.handleChecked.bind(this)} label="Habilitar" />
        </FormGroup>
        <Label for="exampleSelect" className="labelForm" >Enviar una alerta cuando:</Label>
        <Form inline>
        <FormGroup>
        <Label for="exampleSelect" className="mr-sm-2" >La cantidad de {this.state.nameCategory} sea </Label>
        <Input disabled = {!this.state.alert}  type="select"   onClick={this.habdleSelect.bind(this)} name="select" id="exampleSelect">
        <option></option>
        <option>Mayor</option>
        <option>Igual</option>
        </Input>
        <Label for="exampleNumber" className="mr-sm-2 ml-sm-2" > a </Label>
         <Input disabled = {!this.state.alert}  value={this.state.numconficional} min="0" type="number" name="number" id="exampleNumber" placeholder="0" onChange={this.handleChangeNumcon.bind(this)} />
        </FormGroup>
        </Form>
        <FormGroup>
        <Label for = "email" className="labelForm" >Enviar alerta a:< /Label>
         <Input invalid={this.state.expEmailV} disabled = {!this.state.alert} type="text" name = "email" id = "email" placeholder="Correo electrónico" value={this.state.email} onChange={this.handleChangeEmail.bind(this)}  />
         <FormFeedback>El correo electrónico no tiene un formato valido</FormFeedback>
        </FormGroup>
        </div>
       )
     }
   }
   nextStep= () =>{
     if(this.state.category==="" || this.state.category===" " || this.state.name==="" || ( this.state.alert && (this.state.email==="" || this.state.condicional==="none" || this.state.numconficional==="none"))){
       this.setState({
         modal: true,
         titlemodal:"Usuario",
         bodymodal:"Debe ingresar todos los campos para continuar.",
         iconmodal:"warning"
       })

     }else {


       if(this.state.expNameV===false && this.state.expEmailV===false ){

     request
       .post(env.http + "apps")
       .send({
         name:this.state.name,
         devices_id: this.props.idDevice,
         type: this.props.type,
         model: this.state.category,
         image: this.props.image,
         temp: this.props.idtemp,
         idpro:this.props.project.id,
         condi :this.state.condicional,
         numcondi:this.state.numconficional,
         email:this.state.email
       }) // sends a JSON post body
       .end((err, res) => {
         console.log(res)
         if (err){
           console.log(err)
         } else{
           if (res.body.code === 200) {

               store.dispatch({
                 type:"CONFIG_APP",
                 step:2,
                 temp:res.body.message.id_temp
               })
               // sessionStorage.setItem("temp",res.body.message.id_temp)
           }

         }

       })
    }
  }
   }
       render(){

           return (

           <div>
              <Form>
                  <FormGroup >
                    <Label for = "name" className="labelForm" > Nombre < /Label>
                    <Input invalid={this.state.expNameV} maxlength="25" type = "text" name = "name" id = "name"  placeholder = " Nombre de la aplicación " value = {this.state.name} onChange = {this.handleChangeName.bind(this)} />
                    <FormFeedback>Caracter invalido</FormFeedback>
                  </FormGroup>
                  <FormGroup >
                    <Label for = "category" className="labelForm"> Seleccione una categoría < /Label>
                    <Input type="select"   onChange={this.habdleSelect.bind(this)} name="category" id="category">
                    <option> </option>
                    <option>Persona</option>
                    <option>Carro</option>
                    <option>Bicicleta</option>
                    <option>Motocicleta</option>
                    <option>Botella</option>
                    </Input>
                  </FormGroup>
                  {this.hideAlert()}
                  {/* <FormGroup>
                  <Label for="exampleCheckbox" className="labelForm" >Sistema de alerta</Label>
                  <CustomInput type="checkbox" id="1" onChange={this.handleChecked.bind(this)} label="Habilitar" />
                  </FormGroup>
                  <Label for="exampleSelect" className="labelForm" >Enviar una alerta cuando:</Label>
                  <Form inline>
                  <FormGroup>
                  <Label for="exampleSelect" className="mr-sm-2" >La cantidad de {this.state.nameCategory} sea </Label>
                  <Input disabled = {!this.state.alert}  type="select"   onClick={this.habdleSelect.bind(this)} name="select" id="exampleSelect">
                  <option></option>
                  <option>Mayor</option>
                  <option>Igual</option>
                  </Input>
                  <Label for="exampleNumber" className="mr-sm-2 ml-sm-2" > a </Label>
                   <Input disabled = {!this.state.alert}  value={this.state.numconficional} min="0" type="number" name="number" id="exampleNumber" placeholder="0" onChange={this.handleChangeNumcon.bind(this)} />
                  </FormGroup>
                  </Form>
                  <FormGroup>
                  <Label for = "email" className="labelForm" >Enviar alerta a:< /Label>
                   <Input invalid={this.state.expEmailV} disabled = {!this.state.alert} type="text" name = "email" id = "email" placeholder="Correo electrónico" value={this.state.email} onChange={this.handleChangeEmail.bind(this)}  />
                   <FormFeedback>El correo electrónico no tiene un formato valido</FormFeedback>
                  </FormGroup> */}
                  <Row className="mt-3">
                  <Col md="4" ></Col>
                  <Col md="3">
                   {
                     < Button className="mb-4" color="success"  onClick={this.nextStep} size="lg" block>Siguiente</Button>
                   }
                  </Col>
                  <Col md="4" ></Col>
                  </Row>
              </Form>
              <div>
                <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
                  <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
                  <ModalBody>
                    <Row>
                    <Col className="md-2 p-0 text-aling text-center">
                    {this.imagesIcon()}
                    </Col>
                    <Col className="md-10 p-0 mt-4 text-aling text-left">
                      <p className="bodymessa">{this.state.bodymodal}</p>
                    </Col>
                    </Row>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="primary" onClick={this.toggle}>Aceptar</Button>
                  </ModalFooter>
                </Modal>
              </div>
           </div>
           );
         }
   };

   export default ConfigApp;
