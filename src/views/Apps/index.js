import React, {
  Component
} from 'react';
import {
  Row,
  Col,
  Alert,
  Button,
  Modal, ModalHeader, ModalBody, ModalFooter, Table, ButtonGroup,
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,DropdownItem,
  FormGroup,
  Input,
  Label
} from 'reactstrap';
import request from 'superagent'
import {
  Link , Redirect 
} from 'react-router-dom';
import Switch from "react-switch";
import store from '../../store';
import env from "../../env.json";
import './css/appsCard.css'
class AppsCard extends Component {
  constructor() {
    super();
    this.state = {
      camera: [],
      project: store.getState().project,
      checked: false,
      modal:false,
      modalAdver:false,
      modalNewApp:false,
      titlemodal:"",
      bodymodal:"",
      iconmodal:"",
      idtemp:"",
      newAppRoutin:{
     
      }
    }
    this.handleChange = this.handleChange.bind(this);
    this.toggle = this.toggle.bind(this);
    // this.toggleAdver = this.toggleAdver.bind(this);


  }
  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  imagesIcon() {
      if (this.state.iconmodal === "info") {
        return ( < i className = "material-icons infomo" > info < /i>)
        }
        else if (this.state.iconmodal === "warning") {
          return ( < i className = "material-icons icowarni" > warning < /i>)
          }
          else if (this.state.iconmodal === "check_circle_outline") {
            return ( < i className = "material-icons icocheck" > check_circle_outline < /i>)
            }else if(this.state.iconmodal==="error_outline"){
              return (<i className="material-icons iconerror">error_outline</i>)
            }
  }
  componentDidMount() {
    this.cameras();
  }
  /*
  Metodos
  */
  /*Ver todas las camaras*/
  cameras = () => {

    request
      .post(env.http+"allapps")
      .send({
        project_id: this.state.project.id
      }) // sends a JSON post body
      .end((err, res) => {
        // console.log("Resouesta de allapps",res);
        if (err) {
          console.log(err)
        } else {
          if (err) {

          }else {
            if (res.body.code === 200) {
              this.setState({
                camera: res.body.message
              })

            }
          }
        }
      });
  }
  deleApps = (id,power) => {
    if(power){
      this.setState({
        modal: true,
        titlemodal:"Usuario",
        bodymodal:"Señor usuario, recuerde que primero debe apagar la aplicación.",
        iconmodal:"warning"
      })
    }else {
      this.setState({
        modalAdver: true,
        titlemodal:"Usuario",
        bodymodal:"Señor usuario, esta seguro de eliminar la aplicación.",
        iconmodal:"warning",
        idtemp:id
      })
    }
  }
    cancelDele=()=>{
      this.setState({
        modalAdver: false,
      })
    }

    acepConfiDele=()=>{
      this.setState({
        modalAdver: false,
      })
      var user = sessionStorage.getItem("token");
      // var user = this.state.token
      var joinUser = 'token ' + user
      request
        .delete(env.http + "app")
        .set('authorization', String(joinUser))
        .send({
          idApp: this.state.idtemp
        }) // sends a JSON post body
        .end((err, res) => {
          if (err) {

          }else{
            if (res.body.code === 200) {
              this.cameras();
            }
          }
        });

    }

  valipay=(app,valiCamera)=>{
       
   if(app.pay==="pay" && app.vali===false){
        return (<h6>Validando pago</h6>)
    }else{
        return(  <Switch
          height = {20} width = {45}
          disabled={!valiCamera}
      onChange={()=>this.handleChange(app.idApp,app.power)} checked = {app.power}
      uncheckedIcon={
      <div
      style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100%",
      fontSize: 13,
      color:"white",
      paddingRight: 2
      }}
      >
      Off
      </div>
      }
      checkedIcon={
      <div
      style={{
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      height: "100%",
      fontSize: 13,
      color: "white",
      paddingRight: 2
      }}
      >
      On
      </div>
      }
      className="react-switch"
      id="icon-switch"
      />
    )
  }
  }

  handleChange(id, checkedcam) {
    request
      .post(env.http + "powerapp")
      .send({
        idApp: id,
        power: !checkedcam
      }) // sends a JSON post body
      .end((err, res) => {
        // console.log("status",res.status);
          if (res  === undefined  ){
            this.setState({
              modal: true,
              titlemodal:"Usuario",
              bodymodal:"Señor usuario, hubo un error en el servidor por favor intente más tarde.",
              iconmodal:"info"
            })
            this.cameras();
          }else if (res.status === 200) {
            this.cameras();

          }else if (res.status === 501 || res.status === 500 ) {
            this.setState({
              modal: true,
              titlemodal:"Usuario",
              bodymodal:"Señor usuario, hubo un error en el servidor por favor intente más tarde.",
              iconmodal:"info"
            })
            this.cameras();
          }
          if (err) {
  console.log("eerror", err);
          }

      });
  }

toogleSettin=(key)=>{

    this.state[key] = !this.state[key]
    this.setState({
      key : ! this.state[key]
    })
  }

handleChangeRefer(event){
  
  var data = event.target.value.split("*")
 
  this.setState({
    newAppRoutin :{
      pathname:"/industria",
      state:{
        "name": data[0],
        "id":data[1]
      }
    }
  })
}
  render() {
    var Camera = " "
    var bodyTable=[]
    var devices=[]
    if(this.state.camera.length === 0){


       Camera = (<Col className="mt-4">
           <Alert color="dark">

           <p className="noteP">Señor usuario, actualmente no cuenta con ninguna cámara configurada recuerde que primero debe agregar una nueva.</p>
           </Alert>
        </Col>)

    }else {
      
      
      this.state.camera.map((obj, indexC)=>{
        if(obj.valiCamera){

          devices.push(
        <option key={indexC} value={`${obj.name}*${obj.idDevice}`}>{obj.name}</option>
      )
        }
        obj.apps.map((app, index)=>{
                    
          if(app.vali===true ){
            var tempPath = {}
            if (app.type == "POLYGON COUNTER") {
              tempPath = {pathname: `/infcount/${String(app.idApp)}/${String(obj.name)}/${String(app.name)}`,
              state: {
                
                name: "aplica",
                id  : String(app.idApp),
                url: "incometotal"
              }
            }
            } else if (app.type == "INCOME COUNTER") {
              tempPath = {pathname: `/infincome/${String(app.idApp)}/${String(obj.name)}/${String(app.name)}`,
              state: {
                
                name: "aplica",
                id  : String(app.idApp),
                url: "incometotal"
              }
            }
            } else if (app.type == "ANALYTIC AUDIENCES") {
              tempPath = {pathname: `/infanalytic/${String(app.idApp)}/${String(obj.name)}/${String(app.name)}`,
              state: {
                
                name: "aplica",
                id  : String(app.idApp),
                url: "incometotal"
              }
            }
            }
            bodyTable.push(
              <tr>
              <td className="text-center" >{this.valipay(app,obj.valiCamera)}</td>
              <td className="text-center">{obj.name}</td>
              <td className="text-center"> {app.name}</td>
              <td className="text-center">
              
                <Link to={tempPath}>
                < i className = "material-icons iconoApp" >insert_chart_outlined< /i>
                {/* <Button size="lg" style={{"font-size":"10px","width":"100px","height":"35px"}} className="text-center" outline color="info"></Button> */}
                </Link>
              </td>
              <td>
                   <ButtonGroup className="float-center "  >
              <ButtonDropdown className="" id={`${obj.name}-${index}`} isOpen={this.state[`${obj.name}-${index}`] == undefined ? this.state[`${obj.name}-${index}`]= false :  this.state[`${obj.name}-${index}`]} toggle={() => { this.toogleSettin(`${obj.name}-${index}`) }}>
                    <DropdownToggle  className="p-0 iconMore"  > 
                    <i className="material-icons ">more_horiz</i>                      
                    </DropdownToggle>
                    <DropdownMenu right>
                      {/* <DropdownItem onClick={()=>{  this.updateDesk(obj["_id"],obj.state);
                      }}><span>
                        <i class="material-icons" style={{"fontSize":"20px","color":"#000000"}}>
                      autorenew
                      </i>Act. Desktop</span></DropdownItem> */}
                      <DropdownItem onClick={()=>this.deleApps(app.idApp,app.power)}>
                      <i className="material-icons" style={{"fontSize":"20px","color":"#000000"}}>delete_outline</i>
                      Eliminar
                      </DropdownItem>
                      {/* <DropdownItem>Editar</DropdownItem> */}
                    </DropdownMenu>
                  </ButtonDropdown>
                </ButtonGroup></td>
            </tr>
            )
          }

        }) 
      })

      Camera = (
       
        <Table striped responsive>
        <thead>
          <tr>
            <th style={{"width":"150px"}} >Apagado/Encendio</th>
            <th>Cámara</th>
            <th>Aplicación</th>
            <th>Informe</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {bodyTable}
        </tbody>
        </Table>
        
      
      )
  }

    return (
      <div>
  
        <Row className="mb-3" >
        
          <Col md="12" sm="12" className = "clearfix" > <span className="display-4 float-left ">Aplicaciones</span>
          <button onClick={()=> { this.setState({modalNewApp:!this.state.modalNewApp})}} size="lg" style={{"background-color":"#20A8D8","color":"white","margin-top":"20px","width":"160px","height":"47px","font-size":"18px","border-radius": "6px"}}className="btn btn-secondary  float-right">Nueva Aplicación</button>
           </Col >

        </Row>
    
        <Row>
          {Camera}
        </Row>
        <div>
          <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.toggle}>Aceptar</Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal isOpen={this.state.modalAdver}  className={this.props.className}>
            <ModalHeader toggle={this.toggle}>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
              <Row>
              <Col className="md-2 p-0 text-aling text-center">
              {this.imagesIcon()}
              </Col>
              <Col className="md-10 p-0 mt-4 text-aling text-left">
                <p className="bodymessa">{this.state.bodymodal}</p>
              </Col>
              </Row>
            </ModalBody>
            <ModalFooter>
              <Button color="primary" onClick={this.acepConfiDele}>Aceptar</Button>
              <Button color="secondary" onClick={this.cancelDele}>Cancelar</Button>
            </ModalFooter>
          </Modal>
        </div>
        <div>
          <Modal isOpen={this.state.modalNewApp} toggle={()=> { this.setState({modalNewApp:!this.state.modalNewApp})} } className={this.props.className}>
            <ModalHeader style={{"background":"#41A4A4","color":"white"}}className="text-center" toggle={()=> { this.setState({modalNewApp:!this.state.modalNewApp})}}>Seleccione su dispositivo</ModalHeader>
            <ModalBody>
              <Row>
              
              <Col className="md-2 p-0 text-aling text-center">
              <i className="material-icons  iconoCameranew">linked_camera</i>
              </Col>
              </Row>

              <Row>
              <Col md="3"></Col> 
              <Col md="6" className=" p-0 text-aling text-center">
              <FormGroup>
      
      <Input className="selectStyle"  onChange={this.handleChangeRefer.bind(this) } size="5px" type = "select"   name="select" id = "exampleSelect" placeholder="Dispositivo" >
      <option disabled selected key={"index"} value={"obj.rtsp"}>Dispositivo</option>
     {devices}
      </Input>
      </FormGroup>
              </Col>
              <Col md="3"></Col> 
              </Row>
              
            </ModalBody>
            <ModalFooter>
              <Link className="btn outline" style={{"font-size":"18px"}}to={this.state.newAppRoutin}>Continuar</Link>
            </ModalFooter>
          </Modal>
        </div>
      </div>
    )
  }

}

export default AppsCard;
