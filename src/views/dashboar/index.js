import React, { Component , Fragment  } from 'react';
import request from 'superagent'
import store from '../../store';
import env from "../../env.json";
import { Form, FormGroup, Label, Input, Col, Row} from 'reactstrap';
import Select from 'react-select'
import CountingArea from './components/CountingArea.js'
import CountingIncome from './components/CountingIncome.js'
import AnalyticAudience from './components/AnalyticAudiences.js'
import "./css/dash.css"

class Dash extends Component {
  constructor(){
    super()
    this.state={
      appsrun:[],
      appsrunBack:[],
      project:store.getState().project,
      filtersApps:[],
      filtersDevices:[],
      typeApss: [
        { value: 'ANALYTIC AUDIENCES', label: 'Analítica de audiencia' },
        { value: 'INCOME COUNTER', label: 'Conteo en ingresos' },
        { value: 'POLYGON COUNTER', label: 'Conteo en área' },
      ]
    }
  }

  componentDidMount(){
    request
     .post(env.http+"onapps")
     .send({idpro:this.state.project.id})
     .end((err, res) => {
       if (!err && res.body.code===200) {
          this.setState({
            appsrun:res.body.message,
            appsrunBack:res.body.message,
          });
       }
      });
  }
  render(){
    
    this.state.appsrunBack.forEach((element,key )=> {
      this.state.appsrun[key]= Object.assign({},element)
    });
    
    let tempFilter = []

    this.state.appsrun.filter(object=>{
      this.state.filtersDevices.forEach(element => {
        // console.log("elemetn",element,object.cameraName,object.cameraName === element.value);
            if( object.cameraName === element.value){
              tempFilter.push(object)
              return object
          }
      });
  }
)

    const cameras = this.state.appsrun.map((obj,key)=>{
      return {
         value: obj.cameraName, label: obj.cameraName 
      }
    })

    let apps = []
    let arrglo = []
    if(tempFilter.length>0){
        arrglo=tempFilter
    }else{
       arrglo= this.state.appsrun
    }

    arrglo.forEach(element => {
      element.apps.forEach(elementApps => {
        if(elementApps.type=="POLYGON COUNTER" && elementApps.vali){
          apps.push(
            <Fragment>
             <Col xs="12" sm="6" md="4"> 
          <CountingArea id={elementApps.idapp} camera ={element.cameraName[0].toUpperCase()+element.cameraName.slice(1)} host={elementApps.host} port={elementApps.port}  nameApp={elementApps.name} power={elementApps.power} />
            </Col>
          </Fragment>
          )
        }else if(elementApps.type=="ANALYTIC AUDIENCES" && elementApps.vali){
          apps.push(
            <Fragment>
            <Col xs="12" sm="6" md="4"> 
            <AnalyticAudience id={elementApps.idapp} camera ={element.cameraName[0].toUpperCase()+element.cameraName.slice(1)}  host={elementApps.host} port={elementApps.port}  nameApp={elementApps.name} power={elementApps.power}/>
            </Col>
          </Fragment>
            )
        }else if(elementApps.type=="INCOME COUNTER" && elementApps.vali){
          apps.push(
            <Fragment>
            <Col xs="12" sm="6" md="4">
          <CountingIncome id={elementApps.idapp} camera ={element.cameraName[0].toUpperCase()+element.cameraName.slice(1)} host={elementApps.host} port={elementApps.port}  nameApp={elementApps.name} power={elementApps.power}/>
            </Col>
            </Fragment>
            )
        }
      });
  });

    return(
      <div>
        <Row className = "mb-3"> 
        <Col className = "text-center" > 
          <span className="display-4 ">Dashboard</span>
         </Col> 
        </Row>
        <Row className = "mb-3"> 
        <Col md="4"> 
        <Form>
        <FormGroup row>
                    {/* <Col md="3">
                      <Label htmlFor="selectSm">Cámaras</Label>
                    </Col> */}
                    <Col >
                    <Select
                          placeholder="Cámaras"
                          closeMenuOnSelect={true}
                          isMulti
                          options={cameras}
                          onChange={
                            items=>{
                              this.setState({
                                filtersDevices:items}
                               )}
                          }
                         
                    />
                    </Col>
                  </FormGroup>
         </Form>
         </Col> 
         {/* <Col md="6"> 
        <Form>
        <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="selectSm"></Label>
                    </Col>
                    <Col xs="12" md="9">
                          <Select
                          isClearable={true}
                          placeholder="Aplicaciónes"
                          closeMenuOnSelect={false}
                          isMulti
                          options={this.state.typeApss}
                         onChange={
                           items=>{
                             this.setState({
                               filtersApps:items}
                              )}
                         }
                          />
                    </Col>
                  </FormGroup>
         </Form>
         </Col>  */}
        </Row>

        <Row >
          {apps}
          {/* <Col  xs="12" sm="6" md="4">
                <AnalyticAudience host="localhost" port="" nameApp="Analitca Mac Center 93"/>
          </Col>
          <Col  xs="12" sm="6" md="4">
                <CountingArea host="localhost" port="9023" nameApp="Analitca Mac Center 93"/>
          </Col>
          <Col  xs="12" sm="6" md="4">
                <CountingIncome host="localhost" port="9023" nameApp="Analitca Mac Center 93"/>
          </Col> */}
        </Row>  
      </div>  
    )

  }
}
export default Dash;