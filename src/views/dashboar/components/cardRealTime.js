import React, { Component } from "react";
import {Row,Col, Card, CardBody} from 'reactstrap';
import ReactLoading from 'react-loading';
import Switch from "react-switch";
import "../css/dash.css"
// import "./cardbody.css"
// import ProgressBar from "../progressBar";

class Cardb extends Component{
    constructor(){
      super()
      this.state={
        counter:0,
        loader:true,
        switchState:false
      }
      this.handleChange = this.handleChange.bind(this);
    }
    handleChange(){
      if(this.props.power){
        this.setState({
          switchState:!this.state.switchState
        })
      }else{
        this.setState({
          switchState:false
        })
      }

    }

    loader(){
      if(this.state.switchState){
        if(!this.props.loader){
          return (
            <div>

            <Card  className="cardStreaming" body inverse color="info">

            <CardBody className="p-0">
            <Row>
            <Col></Col>
            <Col>
            <ReactLoading type="bars" width={'100%'} />
            </Col>
            <Col></Col>
            </Row>
            </CardBody>
            </Card>
            </div>
          )

        }else{
          return (

            <img className="imgCamera mb-2" ref="imagen" src={this.props.image_src}  alt="visualizacion de IA" />

          )
        }
      }else{
        return (
          <div>
          <Card  className="cardStreaming" body inverse color="info">

          <CardBody className="p-0">
          <Row>
          <Col className="text-center">
            <i class="material-icons iconvideo m-2">videocam</i>
          </Col>
          </Row>
          </CardBody>
          </Card>

          </div>
        )
      }

    }

     render(){
       
        return (
          <div>
          <Col>
          {this.loader()}
          </Col>
          <Col>
          <Row>
          <Col className="text-center">
          <h5>Ver trasmisión</h5>
          </Col>
          <Col className="text-center">
          <Switch
          height = {20} width = {45}
          onChange={()=>this.handleChange()} checked = {this.state.switchState}
          uncheckedIcon={
          <div
          style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 13,
          color:"white",
          paddingRight: 2
          }}
          >
          Off
          </div>
          }
          checkedIcon={
          <div
          style={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          height: "100%",
          fontSize: 13,
          color: "white",
          paddingRight: 2
          }}
          >
          On
          </div>
          }
          className="react-switch"
          id="icon-switch"
          />
          </Col>
          </Row>


          </Col>
          </div>
        )
    }
};

export default Cardb;
