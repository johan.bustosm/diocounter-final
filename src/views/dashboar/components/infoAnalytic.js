import React, { Component } from "react";
import { Pie } from 'react-chartjs-2';
import {ResponsiveContainer,
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,Label as LabelR
} from 'recharts';
import Select from 'react-select'
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardTitle,
  Col,
  Progress,
  Row,
  Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

// const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

import request from 'superagent'
import 'react-dates/initialize';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
import env from "../../../env.json"
import '../css/infocounter.css'
// import 'react-date-range/dist/styles.css'; // main style file
// import 'react-date-range/dist/theme/default.css'; // theme css file
import 'react-dates/lib/css/_datepicker.css';
import {xlsGeneral} from '../../xls/logic'
import moment from 'moment'
moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
  }
)

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')






class informeCountingArea extends Component{
    constructor(props){
      super()
      this.state={
        id_app:"",
        type:"",
        camera:"",
        nameApp:"",
        url:"duration",
        urlGender:"gender",
        urlEmotion:"emotion",
        typeapp:"",
        startDate: moment(new Date(new Date().getFullYear(),new Date().getMonth(),1)),
        endDate:moment(new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate())),
        modal:false,
        titlemodal:"",
        bodymodal:"",
        iconmodal:"",
        focusedInput:null,  
        dateRange: {
          selection: {
            startDate:  new Date(),
            endDate: null,
            key: 'selection',
          },
        },
        timer:0,
        clients:0,
        woman:0,
        man:0, 
        dataGrafic:[ ],
        dataEmotion:{emotion:[],timer:0,category:"-"},
        selectFilterTimer:1,
        selectFilterType:1,
        selectFilterEmo:3,
        categoryType:"total",
        categoryTimer:"mes",
        porcentVent:0,
        portcentCliEmp:0,
        ventas:0,
        emple:0,
        ageAverage:"-"
      }
      this.toggle = this.toggle.bind(this);
    }
    
    
    componentDidMount(){
      this.setState({
        id_app: this.props.match.params._id,
        camera:this.props.match.params.camera,
        nameApp:this.props.match.params.nameApp
        // url:this.props.location.state.url
      })
      this.timerAveraApi()
      this.generalApi()
      this.dataGraficApi( this.state.categoryType,this.state.categoryTimer)
      this.emotionApi("3",false)
      this.averageAge()

    }

    toggle() {
      this.setState({
        modal: !this.state.modal,
        ventas:0,
        emple:0,
        porcentVent:0,
        portcentCliEmp:0
      });
    }

    handleChangeVentas(event){
       this.setState({ventas:event.target.value >= 0 ? event.target.value : this.state.ventas})
    }
    
    handleChangeCliEmp(event){this.setState({   emple:event.target.value >= 0 ? event.target.value : this.state.emple})}
    
    onRadioBtnClick=(number,category, categoryType,categoryTimer)=>{
        if(categoryType){
          if(number!==3){
            this.dataGraficApi( categoryType,this.state.categoryTimer)
          }else{
            this.dataGraficApiAge( categoryType,this.state.categoryTimer)
          }
          
          this.setState({
            categoryType :categoryType
          })
        }else if(categoryTimer){
          if(this.state.categoryType!=="age"){
            this.dataGraficApi( this.state.categoryType,categoryTimer)
          }else{
            this.dataGraficApiAge( this.state.categoryType,categoryTimer)
          }
         
          this.setState({
            categoryTimer :categoryTimer
          })
        }
        if(category === "timer"){
         
          if(number === 1){
           
            this.setState({
              selectFilterTimer : number
            })
          }else if(number === 2){
           
            this.setState({
              selectFilterTimer : number
            })
          }

        }else{
          if(number === 1){
           
                this.setState({
                  selectFilterType:number
                })
          }else if(number === 2){
           
                this.setState({
                  selectFilterType:number
                })
          }else if(number === 3){
           
                this.setState({
                  selectFilterType:number
                }) 
          }
        }
    }

    onRadioBtnClickEmo=(number,category, categoryType,categoryTimer)=>{
          if(number === 1){
            this.emotionApi("1") 
          this.setState({
            selectFilterEmo:number
          })
          }else if(number === 2){
            this.emotionApi("2") 
          this.setState({
            selectFilterEmo:number
          })
          }else if(number === 3){
          this.emotionApi("3",false) 
          this.setState({
            selectFilterEmo:number
          }) 
          }
  }
    
    handleChangeMark (event) {
      if(event.target.value==="Día"){
        this.setState({
          type:"dia"
        })
      }else if(event.target.value==="Hora"){
        this.setState({
          type:"hora"
        })
      }
      else if(event.target.value===" "){
        this.setState({
          type:" "
        })
      }
      }
  
  timerAveraApi=(vali,start,end)=>{
    request
    .post(env.http + "duration")
    .send({
      app:this.props.match.params._id,
      datestart: vali ? start :this.state.startDate,
      dataend:vali ? end :this.state.endDate,
    })
    .end((err, res) => {
      if(res.status === 200){
        this.setState({
          timer : res.body.timer,
        })
      }
    })
  }
  generalApi=(vali,start,end)=>{
    request
    .post(env.http + "gender")
    .send({
      app:this.props.match.params._id,
      datestart: vali ? start :this.state.startDate,
      dataend:vali ? end :this.state.endDate,
    })
    .end((err, res) => {
      if(res.status === 200){
        this.setState({
          clients : res.body.population,
          woman : res.body.woman,
          man : res.body.man,

        })
      }
    })
  }
  dataGraficApi=(type,timer,vali,start,end)=>{
    request
    .post(env.http + "people")
    .send({
      app:this.props.match.params._id,
      datestart: vali ? start :this.state.startDate,
      dataend:vali ? end :this.state.endDate,
      type:type,
      timer:timer,
    })
    .end((err, res) => {
      if(res.status === 200){
        this.setState({
          dataGrafic:res.body.filtro
        })
      }
    })
  }

  dataGraficApiAge=(type,timer,vali,start,end)=>{
    request
    .post(env.http + "age")
    .send({
      app:this.props.match.params._id,
      datestart: vali ? start :this.state.startDate,
      dataend:vali ? end :this.state.endDate,
      type:type,
      timer:timer,
    })
    .end((err, res) => {
      
      if(res.status === 200){
        this.setState({
          dataGrafic:res.body.filtro
        })
      }
    })
  }
  averageAge=()=>{
    request
    .post(env.http + "averageage")
    .send({
      app:this.props.match.params._id,
      datestart: this.state.startDate,
      dataend:this.state.endDate,
    })
    .end((err, res) => {
      
      if(res.status === 200){
        var dato 
        switch (res.body.category) {
          case -1:
            dato="-"
            break;
          case 0: 
            dato="0-12"
          case 1: 
            dato="13-22"
            break; 
          case 2:
           dato="23-32"
            break;
          case 3: 
            dato="33-42" 
          case 4: 
            dato="43-52"
            break; 
          case 5:
            dato="53+"
            break;
          default:
            dato="-"
        }
        this.setState({
          ageAverage:dato
        })
      }
    })
  }
  emotionApi=(gender,vali,start,end)=>{
    request
    .post(env.http + "emotion")
    .send({
      app:this.props.match.params._id,
      datestart:  vali===true ? start :this.state.startDate,
      dataend: vali===true ? end :this.state.endDate,
      genero:parseInt(gender)
    })
    .end((err, res) => {
      
      if(res.status === 200){
        this.setState({
          dataEmotion:{
            emotion:res.body.emotion,
            timer:res.body.timer,
            category:res.body.category
          }
        })
      }
    })
  }

onClickSelectGender=(items)=>{
  
  let tama = items.length
  if(tama ===0 || tama === 2 ){
    this.emotionApi("3",false)
  }else if(tama === 1){
    this.emotionApi(items[0].value)
  }
}

calConver=()=>{
 
  this.setState({
    porcentVent: this.state.ventas === 0 ? 0 :  parseFloat(this.state.ventas/this.state.clients).toFixed(2)*100,
    portcentCliEmp: this.state.emple === 0 ? 0 :  parseFloat(this.state.clients/this.state.emple).toFixed(2)
  })
}

    
  handleRangeChange(which, payload) {
     
     this.setState({
       [which]: {
         ...this.state[which],
         ...payload,
       },
     });
   }

   genderXls=()=>{
    xlsGeneral(
      this.state.camera,
      this.state.nameApp,
      {start:this.state.startDate,end:this.state.endDate},
      this.state.dataGrafic,
      this.state.dataEmotion,
      this.state.selectFilterEmo,
      this.state.categoryType,
      this.state.categoryTimer
    )
   }


     render(){

        return (
         
          <div>
            <Row>
              <Col md="6">
                <span className="display-3 ">Reporte</span>
                <div className="medium text-muted">{`${this.state.camera}  -  ${this.state.nameApp}`}</div>
              </Col>
              
              <Col className="text-right mt-4" md="6">
                  <DateRangePicker
                  startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                  endDatePlaceholderText="Fecha Final"
                  startDatePlaceholderText="Fecha Inicio"
                  endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                  endDateId="home" // PropTypes.string.isRequired,
                  onDatesChange={({ startDate, endDate }) => {
                        this.timerAveraApi(true,startDate, endDate)
                        this.generalApi(true,startDate, endDate)
                        this.dataGraficApi( this.state.categoryType,this.state.categoryTimer,true,startDate, endDate)
                        this.emotionApi("3",true,startDate, endDate)
                        this.averageAge()
                    this.setState({ startDate, endDate })
                  }} // PropTypes.func.isRequired,
                  focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                  onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                  showDefaultInputIcon={true}
                  enableOutsideDays={false}
                  isOutsideRange={() => false}
                  regular={false}
                  />
                   <Button color="primary" className="float-right" style={{"height":"47px","weight":"48px","margin-left":"5px","font-size":"20px"}} onClick={()=>{this.genderXls()}}><i className="icon-cloud-download"></i></Button> 
              </Col>
            </Row>
            <Row className="mt-3">
              <Col>
                <Card>
                <CardBody>
                  <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Datos Globales</CardTitle>
                  </Col>
                  </Row>
                  <Row>
                    <Col className="text-center" md="3">
                      <h5 className="mt-3">Tiempo de permanencia</h5>
                      <p class="leadP">{this.state.timer < 1 ? parseFloat(this.state.timer*60).toFixed(1)+" s": this.state.timer<60 ?  parseFloat(this.state.timer).toFixed(1)+" m" : parseFloat(this.state.timer/60).toFixed(1)+" h"} </p>
                    </Col>
                    <Col className="text-center" md="2">
                      <h5 className="mt-3">Clientes</h5>
                      <p class="leadP">{this.state.clients}</p>
                    </Col>
                    <Col className="text-center" md="2">
                      <h5 className="mt-3">Edad Promedio</h5>
                      <p class="leadP">{this.state.ageAverage}</p>
                    </Col>
                    <Col className="text-center" md="3">
                      <h5 className="mt-3">Género</h5>
                      <ul>
                      <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon-user progress-group-icon"></i>
                          <span className="title">Hombre</span>
                          <span className="ml-auto font-weight-bold">{parseFloat(this.state.man).toFixed(1)}%</span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="primary" value={this.state.man} />
                        </div>
                        </div>
                        <div className="progress-group">
                        <div className="progress-group-header">
                          <i className="icon-user-female progress-group-icon"></i>
                          <span className="title">Mujer</span>
                          <span className="ml-auto font-weight-bold">{parseFloat(this.state.woman).toFixed(1)}%</span>
                        </div>
                        <div className="progress-group-bars">
                          <Progress className="progress-xs" color="danger" value={this.state.woman} />
                        </div>
                      </div>
                      </ul>
                    </Col>
                    <Col className="text-center" md="2">
                      <h5 className="mt-3">Conversiones</h5>
                      <Button  className="mb-3" color="primary" onClick={this.toggle}
                          >Ventas</Button>
                    </Col>
                  </Row>
                 </CardBody>
                </Card>
              </Col>
            </Row>
            <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Clientes</CardTitle>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                   
                    <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                      <ButtonGroup className="mr-3" aria-label="First group">
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1,"timer",undefined,"mes")} active={this.state.selectFilterTimer === 1}>Mes</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2,"timer",undefined,"dia")} active={this.state.selectFilterTimer === 2}>Día</Button>
                      </ButtonGroup>
                    </ButtonToolbar>
                    <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                      <ButtonGroup className="mr-3" aria-label="First group">
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1,"type","total",undefined)} active={this.state.selectFilterType === 1}>Todos</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2,"type","gender",undefined)} active={this.state.selectFilterType === 2}>Género</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(3,"type","age",undefined)} active={this.state.selectFilterType === 3}>Edad</Button> 
                      </ButtonGroup>
                    </ButtonToolbar>
                  
                  </Col>
                </Row>
                {
                  this.state.dataGrafic.length === 0 ?  <div className="piechart text-center ">
                  <h2 className="empity">  ∅</h2>
                </div> :
                <div className="chart-wrapper" style={{ height: 305 + 'px', marginTop: 40 + 'px' }}>
                <ResponsiveContainer width={"100%"} height={305} className="mt-5" >
                <AreaChart
        // width={500}
        // height={400}
        data={this.state.dataGrafic}
        margin={{
          
          top: 10, right: 30, left: 0, bottom: 6,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" >
             <LabelR value="Tiempo" offset={-4} position="insideBottom" />
        </XAxis>
        <YAxis allowDecimals={false} label={{ value: 'Cantidad', angle: -90, position: 'Left' }}/>
        <Tooltip />
        <Area type="monotone" dataKey="Mujer" stackId="1" stroke="#FF5C6C" fill="#FF5C6C" />
        <Area type="monotone" dataKey="Hombre" stackId="1" stroke="#20A5D6" fill="#20A5D6" />
        <Area type="monotone" dataKey="cantidad" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
        <Area type="monotone" dataKey="0-12" stackId="1" stroke="#3DA5DA" fill="#3DA5DA" />
        <Area type="monotone" dataKey="13-22" stackId="1" stroke="#FCBD31" fill="#FCBD31" />
        <Area type="monotone" dataKey="23-32" stackId="1" stroke="#EC4A30" fill="#EC4A30" />
        <Area type="monotone" dataKey="33-42" stackId="1" stroke="#452444" fill="#452444" />
        <Area type="monotone" dataKey="43-52" stackId="1" stroke="#1F9978" fill="#1F9978" />
        <Area type="monotone" dataKey="53+" stackId="1" stroke="#D4E4AA" fill="#D4E4AA" />
      </AreaChart>
      </ResponsiveContainer>
                </div>}
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col>
            <Card>
              <CardBody>
              <Row className="mb-3">
              <Col sm="5">
              <CardTitle className="mb-0">Emociones</CardTitle>
              </Col>
              <Col sm="7" className="d-none d-sm-inline-block">
                    {/* <Button color="primary" className="float-right"><i className="icon-cloud-download"></i></Button> */}
                    <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                      <ButtonGroup className="mr-3" aria-label="First group">
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClickEmo(3)} active={this.state.selectFilterEmo === 3}>Todos</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClickEmo(1)} active={this.state.selectFilterEmo === 1}>Hombre</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClickEmo(2)} active={this.state.selectFilterEmo === 2}>Mujer</Button> 
                      </ButtonGroup>
                    </ButtonToolbar>
                  
              </Col>
              
              </Row>
                  <Row>
                    <Col md="6" className="text-center">
                      {/*<h5>Emociones</h5>*/}
                      <div className="chart-wrapper" >
                      {
                        this.state.dataEmotion.emotion.length>0 ? 
                      <Pie data={
                        {
 
                          labels: [
                            'Alegria',
                            'Neutral',
                            'Tristeza',
                            'Ira',
                            'Sorpresa',
                            'Miedo',
                            'Disgusto'
                          ],
                          datasets: [
                            {
                              data: this.state.dataEmotion.emotion,
                              backgroundColor: [
                                '#1B4F72',
                                '#1A5276',
                                '#2874A6',
                                '#2E86C1',
                                '#3498DB',
                                '#5DADE2',
                                '#4D7A99'
                              ],
                              hoverBackgroundColor: [
                                '#1B4F72',
                                '#1A5276',
                                '#2874A6',
                                '#2E86C1',
                                '#3498DB',
                                '#5DADE2',
                                '#4D7A99'
                              ],
                            }],
                            
                        }
                      }/> :
                      <Pie data={
                        {
 
                          labels: [
                            'Sin datos',
                          ],
                          datasets: [
                            {
                              data: [100],
                              backgroundColor: [
                                '#1B4F72',
                              ],
                              hoverBackgroundColor: [
                                '#1B4F72',
                              ],
                            }],
                            
                        }
                      }/> 
                      }
                      </div>
                    </Col>
                    <Col md="3" className="text-center">
                    <h5>Tiempo de permanencia</h5>
                    <div className="circleInfo">
                    <p class="leadPT">{this.state.dataEmotion.timer < 1 ? parseFloat(this.state.dataEmotion.timer*60).toFixed(1)+" s" : this.state.dataEmotion.timer<60 ? parseFloat(this.state.dataEmotion.timer).toFixed(1)+" m" : parseFloat(this.state.dataEmotion.timer/60).toFixed(2)+" h"}</p>
                    </div>
                    </Col>
                    <Col md="3" className="text-center">
                    <h5>Edad Promedio</h5>
                    <div className="circleInfo">
                    <p class="leadPT">{this.state.dataEmotion.category}</p>
                    </div>
                    </Col>
                  </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <div>
          <Modal isOpen={this.state.modal} centered={true} size="lg" toggle={this.toggle} >
            <ModalHeader>Conversiones</ModalHeader>
            <ModalBody>
              <Row>
                    <Col className="text-center">
                      <Form>
                        <FormGroup>
                        <Label for="name">Ventas Totales</Label>
                        <Input  min="0"  type="number" name="name" id="name"  placeholder="Ventas generadas"  value={this.state.ventas} onChange={this.handleChangeVentas.bind(this)} />
                        
                        <Label className="mt-2" for="name">Numero de Empleados</Label>
                        <Input  min="0" type="number" name="name" id="name"  placeholder="Empleados Activos"  value={this.state.emple} onChange={this.handleChangeCliEmp.bind(this)} />
                        
                        </FormGroup>
                        <div className="text-center"><Button color="primary" onClick={()=>this.calConver()}>Calcular</Button></div>
                      </Form>
                    </Col>
                    <Col className="mr-4">
                          <Row>
                            
                            <Col md="5" className="text-center">
                                <h5>Conversion de Ventas</h5>
                                <div className="circleInfo2">
                                <p class="leadPT2">{this.state.porcentVent}%</p>
                                </div>
                            </Col>
                            <Col md="5" className="text-center mr-4">
                              <h5> Clientes por Empleado </h5>
                              <div className="circleInfo2">
                              <p class="leadPT2">{this.state.portcentCliEmp}</p>
                              </div>
                            </Col>
                          </Row>
                    </Col>
              </Row>
            
            </ModalBody>
          </Modal>
        </div>
          </div>
        )
    }
};

export default informeCountingArea;
