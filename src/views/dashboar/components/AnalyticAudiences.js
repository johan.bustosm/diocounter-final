import React, { Component } from 'react';
import { Card, CardBody, CardFooter, Col, Row,Modal, ModalHeader, ModalBody, ModalFooter,Button} from 'reactstrap';
import io from 'socket.io-client';
import {Link} from 'react-router-dom'
import ReactLoading from 'react-loading';
import request from 'superagent'
import env from "../../../env.json"
import "../css/dash.css"
let countInten = 0

class Dash extends Component {
  constructor(){
    super()
    this.state={
     playStream:false,
     men:0,
     woman:0,
     image_src:"",
     loader:false,
     acti_image:false,
     modal:false,
     titlemodal:"",
     bodymodal:"",
     on:false,
     onApp:false,
     blockOn:false,
     routeinfo:{},
     streaming:0

    }
  }


  componentDidMount(){
    this.setState({
      routeinfo: {pathname: `/infanalytic/${String(this.props.id)}/${String(this.props.camera)}/${String(this.props.nameApp)}`,
        state: {
          
          name: "aplica",
          id  : String(this.props.id),
          url: "incometotal"
        }
      }
    })
    if(this.props.port !==undefined){
        this.clienteSocket = io(`http://${this.props.host}:${this.props.port}/brows`, 
          {
            'reconnection': true,
            'reconnectionDelay': 1000,
            'reconnectionDelayMax' : 5000,
            'reconnectionAttempts': 10
        }
        );
        /* listen event*/
        this.clienteSocket.on("time",message=>{
          this.setState({
            streaming: message.state,
          });
        })

        this.clienteSocket.on("count", message => {
          // console.log(message);
          if(this.props.id===message.id){
            if(message.result.men !== this.state.men || message.result.woman !== this.state.woman){
                this.setState({
                    men: message.result.men,
                    woman:message.result.woman
                });
              }

            this.setState({
              image_src:"data:image/jpeg;base64," + String(message.frame)
            });
            if (message.frame !== ""){
              this.setState({
                loader:false,
                acti_image:true
              });
            }else{
                this.setState({
                    loader:true,
                    acti_image:false
                  });
            }
          }
        })

        this.clienteSocket.on('connect_timeout', message=>{
            this.setState({
                playStream:false,
                acti_image:false,
                loader:false
            })
        })

        this.clienteSocket.on('connect_error', message=>{
            countInten += 1 
            if(countInten===5){
                this.setState({
                    playStream:false,
                    acti_image:false,
                    loader:false
                })
            }

        })
        
      }

  }

  toggle=()=> {
    this.setState({
      modal: !this.state.modal,
      blockOn:false
    });
  }

  onApp=()=>{
    this.setState({
        onApp:true,
        blockOn:true
    })  
    request
    .post(env.http + "powerapp")
    .send({
      idApp: this.props.id,
      power: true
    }) // sends a JSON post body
    .end((err, res) => {
      // console.log("status",res.status);
        if (res  === undefined  ){

          this.setState({
            titlemodal:"Error al encendido de la aplicación",
            bodymodal:"Señor usuario, hubo un error en el servidor por favor intente más tarde.",
            onApp:false,
          })
          
        }else if (res.status === 200) {
            this.setState({
                modal:false,
                playStream:true,
                loader:true
            })
          

        }else if (res.status === 501 || res.status === 500 ) {
          this.setState({
            titlemodal:"Error al encendido de la aplicación",
            bodymodal:"Señor usuario, hubo un error en el servidor por favor intente más tarde.",
            onApp:false
          })
          this.cameras();
        }
        if (err) {
            console.log("eerror", err);
        }

    });
  }
  prueba=()=>{
    this.setState({
        acti_image : ! this.state.acti_image,
        loader: ! this.state.loader
    })
  }
  video=(state)=>{
    if(this.props.port !== undefined){
        if(state==="play"){
            this.clienteSocket.emit('streaming')
            this.setState({
                playStream : ! this.state.playStream,
                loader: ! this.state.loader,
            })
        }else{
          this.clienteSocket.emit('streaming') 
            this.setState({
                playStream : ! this.state.playStream,
                loader: false,
                acti_image:false
            })
        }
    }else{
        this.setState({
            modal:true,
            titlemodal:"Aplicación no encendida",
            bodymodal:`Recuerde encender su aplicación ${this.props.nameApp}`,
        })
    }  
  }

  controlVideo(){
      if(!this.state.playStream){
        return(<i className="material-icons icono_play" onClick={()=>this.video("play")} >play_arrow</i>)
      }
      if(this.state.loader){
        return(<ReactLoading className="loaderSpin" type="spin" color={"black"} height={50} width={50} /> )
      }
      if(this.state.acti_image){
         return(<img className="imgCamera" ref="imagen" src={this.state.image_src}  alt="visualizacion de IA" />) 
      }
  }

  render(){

    return(
      <div>
          <Card>
              <CardBody >
               <div className="text-center container_play">
                    {this.controlVideo()}              
              </div>

              </CardBody>
              <CardFooter>
                <Row className="mb-2">
                  <Col>
                  <span className="titleTar">{`${this.props.camera} - ${this.props.nameApp}`}</span>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    {this.state.playStream ? <i className="material-icons iconostop " onClick={this.video} >pause</i> : <div></div>}
                    {/* <Link to={this.state.routeinfo}><i className="material-icons iconosintro iconostop">assessment</i></Link> */}
                    {this.props.power ?
                      this.state.streaming === 1 ? <i className="material-icons iconoStreamingG ">surround_sound</i> :
                      this.state.streaming === 2 ? <i className="material-icons iconoStreamingB ">surround_sound</i> :
                      this.state.streaming === 3 ? <i className="material-icons iconoStreamingR ">surround_sound</i> :
                      <div></div>:<div></div>}
                    
                    
                  </Col>
                  <Col >
                  </Col>
                  <Col>
                  <img ref="imagen" src="h.svg"  alt="imagen de la camra" /> <span className="mr-2 numberStreaming">{this.state.men}</span>
                  <img ref="imagen" src="m.svg"  alt="imagen de la camra" /> <span className="numberStreaming">{this.state.woman}</span>
                  </Col>
                </Row>
                   
              </CardFooter>
            </Card>
            <div>
          <Modal isOpen={this.state.modal}>
            <ModalHeader>{this.state.titlemodal}</ModalHeader>
            <ModalBody>
            <p className="bodymessa">{this.state.bodymodal}</p>
            </ModalBody>
            <ModalFooter>
              {/* <Button disabled={this.state.blockOn} color="primary" onClick={this.onApp}> {this.state.onApp ? <ReactLoading className="loaderSpinButton" type="spin" color={"white"} height={8} width={12} />  : <div></div>} Encender</Button> */}
              <Link to="/apps">  <Button color="primary">  Encender</Button> </Link>   
              <Button disabled={this.state.onApp}color="secundary" onClick={this.toggle}>Cerrar</Button>
            </ModalFooter>
          </Modal>
        </div>
      </div>  

    )

  }
}
export default Dash;