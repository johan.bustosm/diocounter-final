import React, { Component } from "react";
import { Pie } from 'react-chartjs-2';
import {ResponsiveContainer,Legend,
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,LineChart,Line,
  Brush
} from 'recharts';
import Select from 'react-select'
import {
  Button,
  ButtonGroup,
  ButtonToolbar,
  Card,
  CardBody,
  CardTitle,
  Col,
  Progress,
  Row,
  Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities'

// const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

import request from 'superagent'
import 'react-dates/initialize';
import { DateRangePicker, SingleDatePicker, DayPickerRangeController } from 'react-dates';
import env from "../../../env.json"
import '../css/infocounter.css'
// import 'react-date-range/dist/styles.css'; // main style file
// import 'react-date-range/dist/theme/default.css'; // theme css file
import 'react-dates/lib/css/_datepicker.css';
import moment from 'moment'
moment.lang('es', {
  months: 'Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre'.split('_'),
  monthsShort: 'Enero._Feb._Mar_Abr._May_Jun_Jul._Ago_Sept._Oct._Nov._Dec.'.split('_'),
  weekdays: 'Domingo_Lunes_Martes_Miercoles_Jueves_Viernes_Sabado'.split('_'),
  weekdaysShort: 'Dom._Lun._Mar._Mier._Jue._Vier._Sab.'.split('_'),
  weekdaysMin: 'Do_Lu_Ma_Mi_Ju_Vi_Sa'.split('_')
  }
)

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandWarning = getStyle('--warning')
const brandDanger = getStyle('--danger')




class informeCountingArea extends Component{
    constructor(props){
      super()
      this.state={
        id_app:"",
        type:"",
        camera:"",
        nameApp:"",
        startDate: moment(new Date(new Date().getFullYear(),new Date().getMonth(),1)),
        endDate:moment(new Date(new Date().getFullYear(),new Date().getMonth(),new Date().getDate())),
        modal:false,
        titlemodal:"",
        bodymodal:"",
        iconmodal:"",
        focusedInput:null,  
        dateRange: {
          selection: {
            startDate:  new Date(),
            endDate: null,
            key: 'selection',
          },
        },
        cantidad:0,
        timer:0,
        alerts:0,
        dataGrafic : [],
        dataGrafic2:[ { name: '28/3/2019', m: 59.083333333333336 },
        { name: '29/3/2019', m: 60 },
        { name: '30/3/2019', m: 60 }],
        selectFilterTimer:1,
        categoryTimer:"dia",
        porcentVent:0,
        portcentCliEmp:0,
        ventas:0,
        emple:0
      }
      this.toggle = this.toggle.bind(this);
    }
    
    
    componentDidMount(){
      this.setState({
        id_app: this.props.match.params._id,
        camera:this.props.match.params.camera,
        nameApp:this.props.match.params.nameApp
        // url:this.props.location.state.url
      })
      this.dataGraficApi(this.state.categoryTimer)
      this.dataAllObjects(false)
     

    }

    toggle() {
      this.setState({
        modal: !this.state.modal,
        ventas:0,
        emple:0
      });
    }

    onRadioBtnClick=(number,category, categoryType,categoryTimer)=>{
        if(categoryType){
          this.dataGraficApi( categoryType,this.state.categoryTimer)
          this.setState({
            categoryType :categoryType
          })
        }else if(categoryTimer){
          this.dataGraficApi(categoryTimer)
          this.setState({
            categoryTimer :categoryTimer
          })
        }
        if(category === "timer"){
         
          if(number === 1){
           
            this.setState({
              selectFilterTimer : number
            })
          }else if(number === 2){
           
            this.setState({
              selectFilterTimer : number
            })
          }

        }else{
          if(number === 1){
           
                this.setState({
                  selectFilterType:number
                })
          }else if(number === 2){
           
                this.setState({
                  selectFilterType:number
                })
          }else if(number === 3){
           
                this.setState({
                  selectFilterType:number
                }) 
          }
        }
    }

    
    handleChangeMark (event) {
      if(event.target.value==="Dia"){
        this.setState({
          type:"dia"
        })
      }else if(event.target.value==="Hora"){
        this.setState({
          type:"hora"
        })
      }
      else if(event.target.value===" "){
        this.setState({
          type:" "
        })
      }
      }
  

  dataGraficApi=(timer,vali,start,end)=>{
    request
    .post(env.http + "averague")
    .send({
      app:this.props.match.params._id,
      datestart: vali ? start :this.state.startDate,
      dataend:vali ? end :this.state.endDate,
      type:timer
    })
    .end((err, res) => {
      if(res.status === 200){
        this.setState({
          dataGrafic:res.body.filtro,
          timer:res.body.timer
        })
      }
    })
  }

  dataAllObjects=(vali,start,end)=>{
    request
    .post(env.http + "allobjects")
    .send({
      app:this.props.match.params._id,
      datestart: vali ? start :this.state.startDate,
      dataend:vali ? end :this.state.endDate,
    })
    .end((err, res) => {
      if(res.status === 200){
        this.setState({
          cantidad:res.body.counter
        })
      }
    })
  }

    
  handleRangeChange(which, payload) {
     // console.log(which, payload);
     this.setState({
       [which]: {
         ...this.state[which],
         ...payload,
       },
     });
   }


     render(){

        return (
         
          <div>
            <Row>
              <Col md="4">
                <span className="display-3 ">Reporte</span>
                <div className="medium text-muted">{`${this.state.camera}  -  ${this.state.nameApp}`}</div>
              </Col>
              <Col md="4"></Col>
              <Col className="text-right mt-4" md="4">
                  <DateRangePicker
                  startDate={this.state.startDate} // momentPropTypes.momentObj or null,
                  endDatePlaceholderText="Fecha Final"
                  startDatePlaceholderText="Fecha Inicio"
                  endDate={this.state.endDate} // momentPropTypes.momentObj or null,
                  endDateId="home" // PropTypes.string.isRequired,
                  onDatesChange={({ startDate, endDate }) => {
                        this.dataGraficApi(this.state.categoryTimer,true,startDate, endDate)
                        this.dataAllObjects(true,startDate, endDate)
                    this.setState({ startDate, endDate })
                  }} // PropTypes.func.isRequired,
                  focusedInput={this.state.focusedInput} // PropTypes.oneOf([START_DATE, END_DATE]) or null,
                  onFocusChange={focusedInput => this.setState({ focusedInput })} // PropTypes.func.isRequired,
                  showDefaultInputIcon={true}
                  enableOutsideDays={false}
                  isOutsideRange={() => false}
                  regular={false}
                  />
              </Col>
            </Row>
            <Row className="mt-3">
              <Col>
                <Card>
                  <Row>
                    <Col className="text-center" md="4">
                      <h5 className="mt-3">Total de Objectos</h5>
                      <p class="leadP">{this.state.cantidad} </p>
                    </Col>
                    <Col className="text-center" md="4">
                      <h5 className="mt-3">Tiempo de Permanencia</h5>
                      <p class="leadP">{this.state.timer} m</p>
                    </Col>
                    <Col className="text-center" md="4">
                      <h5 className="mt-3">Alertas</h5>
                      <Row>
                          <Col className="text-right" style={{"padding":"0px"}}>
                          <p class="leadP">{this.state.alerts}</p>
                          </Col>
                          <Col className="text-left" style={{"margin":"0px","padding":"0px"}}>
                                <i class="material-icons iconoAler">
                                notifications
                                </i>
                              
                          </Col>
                      </Row>

                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
            <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Duración</CardTitle>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                    {/* <Button color="primary" className="float-right"><i className="icon-cloud-download"></i></Button> */}
                    <ButtonToolbar className="float-right" aria-label="Toolbar with button groups">
                      <ButtonGroup className="mr-3" aria-label="First group">
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(1,"timer",undefined,"dia")} active={this.state.selectFilterTimer === 1}>Día</Button>
                        <Button color="outline-secondary" onClick={() => this.onRadioBtnClick(2,"timer",undefined,"hora")} active={this.state.selectFilterTimer === 2}>Hora</Button>
                      </ButtonGroup>
                    </ButtonToolbar>
                  </Col>
                </Row>
                {
                  this.state.dataGrafic.length === 0 ?  <div className="piechart text-center ">
                  <h2 className="empity">  ∅</h2>
                </div> :
                
                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                <ResponsiveContainer width={"100%"} height={300} className="mt-5" >


             <LineChart  data={this.state.dataGrafic}
  margin={{
    top: 10, right: 30, left: 10, bottom: 10,
  }}>
  <CartesianGrid strokeDasharray="3 3" />
  <XAxis dataKey="name" />
          
             <YAxis allowDecimals={false} label={{ value: 'Minuto (m)', angle: -90, position: 'insideLeft' ,offset: 0}}/>
             <Tooltip />
            <Line type="monotone" dataKey="m" stroke="#8884d8" />
</LineChart>
      </ResponsiveContainer>
                </div>}
              </CardBody>
            </Card>
          </Col>
        </Row>
          </div>
        )
    }
};

export default informeCountingArea;


