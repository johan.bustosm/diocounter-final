export default {
  items: [

    {
        name: 'Proyectos',
        url: '/project',
        icon: 'icon-folder',
    }
    ,
    {name: 'Dispositivos',
    icon: 'icon-feed',
    children: [
      {
        name: 'Cámaras',
        url: '/camera',
        icon: 'icon-camera'
      },]
    }
    ,{
      name: 'Aplicaciones',
      url: '/apps',
      icon: 'icon-rocket',
    },
    {
      name: 'Dashboard',
      // url: '/dashboard',
      url: '/dash',
      icon: 'icon-speedometer',
    /*  badge: {
        variant: 'info',
        text: 'NEW',
      },*/
    },

  ]
};
