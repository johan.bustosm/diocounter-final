import React from 'react';
import Loadable from 'react-loadable'
import DefaultLayout from './containers/DefaultLayout';

function Loading() {
  return <div>Loading...</div>;
}
const Dashboard = Loadable({
  loader: () => import('./views/Dashboard'),
  loading: Loading,
});
const ConfigWebCamera = Loadable({
  loader: () => import('./views/ConfigCamera/ConfigWebCam'),
  loading: Loading,
});
const ConfigIPCamera = Loadable({
  loader: () => import('./views/ConfigCamera/ConfigIpCam'),
  loading: Loading,
});
const Counter = Loadable({
  loader: () => import('./views/Counter/counter'),
  loading: Loading,
});
const Project = Loadable({
  loader: () => import('./views/Project/index.js'),
  loading: Loading,
});
const Camera = Loadable({
  loader: () => import('./views/Cameras/index.js'),
  loading: Loading,
});
const newCamera = Loadable({
  loader: () => import('./views/Cameras/newCamera.js'),
  loading: Loading,
});
const appsCard = Loadable({
  loader: () => import('./views/Apps/index.js'),
  loading: Loading,
});
const newapp = Loadable({
  loader: () => import('./views/Apps/views/newApps.js'),
  loading: Loading,
});
const typeapp = Loadable({
  loader: () => import('./views/Apps/views/typeApp.js'),
  loading: Loading,
});
const industria = Loadable({
  loader: () => import('./views/Apps/views/industria.js'),
  loading: Loading,
});
const dash = Loadable({
  loader: () => import('./views/dashboar/index.js'),
  loading: Loading,
});
const test = Loadable({
  loader: () => import('./views/dashboar/test.js'),
  loading: Loading,
});
const informeCountingArea = Loadable({
  loader: () => import('./views/dashboar/components/infocountObj.js'),
  loading: Loading,
});
const informeIncome = Loadable({
  loader: () => import('./views/dashboar/components/infoincome.js'),
  loading: Loading,
});
const informeAnalytic = Loadable({
  loader: () => import('./views/dashboar/components/infoAnalytic.js'),
  loading: Loading,
});

const routes = [
  { path: '/', exact: true, name: 'Inicio', component: DefaultLayout },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/config/webcamera', exact: true, name: 'config web camera', component: ConfigWebCamera },
  { path: '/config/ipcamera', exact: true, name: 'config ip camera', component: ConfigIPCamera },
  { path: '/project', exact: true, name: 'Proyectos', component: Project },
  { path: '/app/counter', exact: true, name: 'app counter', component: Counter },
  { path: '/camera', exact: true, name: 'Dispositivos', component: Camera },
  { path: '/newcamera', exact: true, name: 'Nueva cámara', component: newCamera },
  { path: '/apps', exact: true, name: 'Aplicaciones', component: appsCard },
  { path: '/newapp', exact: true, name: 'Configuración de la aplicación', component: newapp },
  { path: '/industria', exact: true, name: 'Industria ', component: industria },
  { path: '/typeapp', exact: true, name: 'Tipo de app ', component: typeapp },
  { path: '/dash', exact: true, name: 'Dashboard', component: dash },
  { path: '/test', exact: true, name: 'Dashboard', component: test },
  { path: '/infcount/:_id/:camera/:nameApp', exact: true, name: 'Dashboard', component: informeCountingArea },
  { path: '/infincome/:_id/:camera/:nameApp', exact: true, name: 'Dashboard', component: informeIncome },  
  { path: '/infanalytic/:_id/:camera/:nameApp', exact: true, name: 'Dashboard', component: informeAnalytic },
];

export default routes;
